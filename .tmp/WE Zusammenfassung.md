| Begriff     | Erklärung                                                 | Begriff      | Erklärung                                             |
| ----------- | --------------------------------------------------------- | ------------ | ----------------------------------------------------- |
| DOM         | Document Object Model,<br />Schnittstelle HTML JavaScript | D3<br />JSON | Data Driven Documents<br />JavaScript Object Notation |
| Pixel / Pel | Picture Elements                                          | MVC          | Model View Controller                                 |
| Widget      | Window Gadget                                             | Flexbox      | Flexible Box Layout                                   |



## 0. Grundlagen

```javascript
parseInt(str)		// str in int umwandeln
parseFloat(str)		// str in float umwandeln
```

**HTML**

```html
<table>
    <tr>	<th></th>	<th></th>	</tr>
    <tr>	<td></td>	<td></td>	</tr>
</table>
```

**CSS**

```css
CSS in HTML-Datei					CSS in externer Datei
<style type="text/css">				<link rel="stylesheet" type="text/css"
    <!-- /* CSS-Code */ -->			 href="formate.css">
</style>

h1, h2 {color:red ; font-size:36}		/* mehreren Elemente, mehrere Formatierungen */ 

li p {color:green}						/* Absätze in Listenelement sind grün */

p[align] {font-style:italic}			/* zentrierte Absätze sind schräggestellt */

p.hinterlegt {background-color:blue}	/* Absätze mit Klasse "hinterlegt" */
.hinterlegt  		''					/* Alle Elemente mit Klasse "hinterlegt" */

#bereich {position:absolute}			/* Individualformat (id) */

p { margin: 12pt 3pt 6pt 9pt}			/* Abstand von Text zum Rest im Uhrzeigersinn */
```

<img src="../../Typora.assets/cHqTR.png" alt="https://i.stack.imgur.com/cHqTR.png" style="zoom: 33%;" />

<div style="page-break-after: always; break-after: page;"></div>

## 1. Einführung in JavaScript (17-)

```html
<script src="script.js"></script>		<!-- Skript einbinden und direkt ausführen -->
<script src="script.js" async></script>	<!-- Skript einbinden und parallel ausführen -->
<script src="script.js" defer></script>	<!-- Skript einbinden und am Ende ausführen -->
```

```javascript
"use strict";		// Variablen-Deklaration nur mit var möglich

if (window.navigator.userAgent.indexOf("Firefox") > -1){	// Abfrage Browser-Typ
	document.write("Sie benutzen den Firefox <br>");
}

switch (variable){
    case "wert1":
        // Anweisung 1 ;
    case "wert2":
        // Anweisung 2 ;
    default:
        // DefaultAnweisung;
}

document.write("<br>myVar: " + typeof myVar);			// myVar: number

for(var i=0; i<5; i++){..}					// Zählvariable auch außerhalb gültig
for(let i=0; i<5; i++){..}					// Zählvariable nicht außerhalb gültig

number(" 3.7")		/*liefert 3.7 */		number(true)		// liefert 1
```



| a === b                                 | a !== b                                          |
| --------------------------------------- | ------------------------------------------------ |
| Strikt gleich: identischer Typ und Wert | Strikt ungleich: entweder Typ oder Wert ungleich |



| Boolean    | Number           | Object | String              | Undefinded | Null |
| ---------- | ---------------- | ------ | ------------------- | ---------- | ---- |
| true/false | 64-Bit Kommazahl | Object | UTF-16 Zeichenkette | ohne Wert  | leer |
|            |                  |        |                     |            |      |

<div style="page-break-after: always; break-after: page;"></div>

## 2. Array und Funktionen

```javascript
var arr = [23, "String", 14.7, -3];
arr.length								// Feldlänge
var length = arr.push(.., .., ..);		// Elemente .. an Ende einfügen, liefert Länge
arr.shift();							// liefert und entfernt erstes Element
arr.pop();								// liefert und entfernt letztes Element
```

**Assoziatives Feld**

```javascript
var data["Nr.1"] = -27;			for(var key in data){					// Nr.1: -27
data["Nr.2"] = 5;					write(key + ": " + data[key]);		// Nr.2: 5
data["Nr.3"] = 13;				}										// Nr.3: 13
```

**Mehrdimensionales Feld**

```javascript
var arr = [[1, 2, 3],		arr.length			// Anzahl Zeilen
           [4, 5],			arr[0].length		// Anzahl Spalten in Zeile
           [6, 7, 8],		arr[4] = [];		// neue Zeile erzeugen
           [9]];			arr[4].push(10,11);	// neue Zeile befüllen
```

**Funktion**

```javascript
function maximumsuche() {							// Variable Parameter-Anzahl
	var max = 0;
	for (var i = 0; i < arguments.length; i++) {
		if (arguments[i] > max)
			max = arguments[i];
	} return max;
}

var m = maximumsuche;								// m ist nun selbe Funktion
```

```javascript
var a;					// global gültig
function fkt(){
    var b;				// lokal gültig
    a = 1;				// global gültig
    c = 2;	}			// im nicht-strikten Modus global gültig (kein var nötig)
```

**Closure**

```javascript
var scope = "global";						var scope = "global";
function f() { return scope; }				function checkscope() {
function checkscope() {							var scope = "local";
	var scope = "local";						function f() { return scope; }
	return f();								return f();
} checkscope();			/* "global" */		} checkscope();			/* local */
```

<div style="page-break-after: always; break-after: page;"></div>

**Anonyme Funktion**

```javascript
function counter() {			var count = counter();
	var i = 0;					writeln(count());			// 1
	return function() {			writeln(count());			// 2
		return ++i; }			writeln(count());			// 3
}								writeln(counter());			// function(){return ++i;}
```

**Innere Funktion**

```javascript
function outer(a) {					
	return function(b, c) {			writeln(outer(1)(2, 3));
		return a+b+c; }			}	// liefert 6
```

**Gültigkeitsbereich-Funktion**

```javascript
(function(){
    // Lokale Variablen und Funktionen
}());
```



## 3. Objekte

```javascript
var objekt = {attr1:wert1, attr2:wert2, ...};
objekt.attr1 / objekt["attr1"]						// Zugriff auf Objekt-Eigenschaft

for(attr in objekt){
    document.write(attr + objekt.attr);		// Zeigt alle Attribut-Namen und -Werte an
    document.write(attr + objekt[attr]);
}
objekt.attr3 = wert3						// Neue Eigenschaft während Laufzeit
delete objekt.attr3 = wert3					// Eigenschaft löschen
```

**Objektliteral und Methode**

```javascript
var objekt = {attr:wert,
              fkt:function(){return this.attr}	// Methode als anonyme Funktion
             };

var Funktion = function(){this.attr};			// Funktion außerhalb deklarieren
var objekt = {attr:wert,
             fkt:Funktion						// Funktion als Methode nutzen
             };
var duplikat = Object.create(objekt);			// vollständige Kopie von objekt

var objekt = {init:function(attr1, attr2){
             	this.attr1 = attr1;				// "Klasse" erzeugen
              	this.attr2 = attr2;}
             };
var objekt1 = Object.create(objekt);			// Objekt dieser "Klasse" erzeugen
objekt1.init(wert1, wert2);						// Objekt dieser "Klasse" inizialisieren
```

<div style="page-break-after: always; break-after: page;"></div>

**Konstruktor**

```javascript
function Objekt(attr1, attr2){
    this.attr1 = attr1;
	this.attr2 = attr2;
    this.funkt = function(){return this.attr1;};
}
var o1 = new Objekt(wert1, wert2);
document.write(o1.funkt());
```

**Klasse**

```javascript
class Objekt {
    constructor(attr1, attr2){
        this.attr1 = attr1;
		this.attr2 = attr2;
    }
    funkt(){return this.attr1;}
}
var o1 = new Objekt(wert1, wert2);
document.write(o1.funkt());
```

| new Objekt();      | new Date();  | new Array();     |
| ------------------ | ------------ | ---------------- |
| Leeres Objekt = {} | Datumsobjekt | Leeres Feld = [] |

**Komposition**

```javascript
function Car(){
    this.engine = new Engine();
    this.leftDoor = new Door("left");
    this.wheels = [new Wheel(), new Wheel()];
}
function Engine(){};
function Wheel(){};
function Window(){};
function Door(which){
    this.window = new Window();
    this.which = which;
}
```

**Vererbung**

```javascript
function Car(name){this.name = name;}
german_car.prototype = new Car();					// Prototyp erzeugen
var BMW = new german_car("BMW");

VW.prototype = new german_car("VW");
var id3 = new VW("id3");

var firm = Object.getPrototypeOf(id3);
document.write(firm.name);							// VW
```

<div style="page-break-after: always; break-after: page;"></div>

## 4. [Canvas](https://www.w3schools.com/tags/ref_canvas.asp)

```html
<head>		<script src="script.js" defer></script>		</head>		<!-- defer wichtig -->
<body>		<canvas id="myCanvas" width="500" height="500">
        		<p>Leider kein Canvas verfügbar</p>
			</canvas>		</body>
```

```javascript
var c = document.getElementById("myCanvas");			// Canvas erstellen
var ctx = c.getContext("2d");

ctx.beginPath();										// Neue Zeichnung

ctx.rect(x,y,w,h);										// Rechteck
var lgd = ctx.createLinearGradient(xs,ys,xe,ye);		// Linearer Farbverlauf Start End
lgd.addColorStop(stop, "color");						// Bei stop(0-1) ist color

ctx.arc(x,y,r,sA,eA);									// Kreis (sAngle auf x-Achse)
var rgd = ctx.createRadialGradient(ix,iy,ir,ax,ay,ar);	// Farbverlauf zwischen Kreisen
rgd.addColorStop(stop, "color");						// Bei stop(0-1) ist color

ctx.moveTo(sx,sy);  ctx.lineTo(ex,ey)					// Line von s nach e
ctx.lineWidth = …;										// Linen-Dicke

ctx.strokeStyle = "…";	z.B. = lgd oder = rgd			// Linien-Farbe
ctx.fillStyle = "…";									// Füll-Farbe

ctx.stroke();		ctx.fill();							// (un)gefüllt zeichnen
```

**Bild**

```javascript
var img = new Image();												// Bild laden
img.src = "Bild.jpg";
img.onload = function(){	context.drawImage(img, 200, 50);	}	// Bild anzeigen
img.onload = function(){
    var pat=context.createPattern(this, "repeat");					// Textur zeichnen
    context.fillStyle = pat;
    context.fillRect(x,y,width,hight);
}
```

```html
<body>
	<img style="display:none" id="myImage" src="einBild.jpg">		// Textur zeichnen
	<canvas id="cva" width="300" height="200"></canvas>
	<script>
		var canvas = document.getElementById("cva");
		var context = canvas.getContext("2d");
		var img=document.getElementById("myImage");
		var pat=context.createPattern(img,"repeat");
		context.fillStyle=pat;
		context.fillRect(0,0,300,200);
	</script>
</body>
```

<div style="page-break-after: always; break-after: page;"></div>

## 5. Graphics

**SVG**

```html
<svg width="…" height="…" transform="scale(…,…)">
	<rect width="…" height="…" x="…" y="…" />
   	<circle cx="…" cy="…" r="…" />
    <path x1="…" y1="…" x2="…" y2="…"/>
    <polygon />
    <text />
    <image />
    
    < FORM style="stroke-width:…; opacity:…; fill:#………; fill-opacity:…; stroke:#………; stroke-opacity:…" stroke="…" stroke-width="…" fill="…"/>
</svg>
```

**Chartist**

| Größenverhältnis    | 9 : 16             | 3 : 4               | 1 : 1.618           |
| ------------------- | ------------------ | ------------------- | ------------------- |
| **Containerklasse** | `ct-minor-seventh` | `ct-perfect-fourth` | `ct-golden-section` |

```html
<head>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
	<script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <style type="text/css"> … </style>
</head>
<body>
	<div class="mychart ct-perfect-fourth"></div>
    <script> 
		var data = { … };
        var options = { … };
        new Chartist.…('.mychart', data, options);
	</script>
</body>
```

**style**

```css
.ct-series-a .ct-line, .ct-series-a .ct-point {stroke: green;}	/* Line-/Punkte-A grün */
.ct-series-b .ct-line {stroke-width: 1p; stroke: red;}
.ct-series-c .ct-point {stroke-linecap: square;}	/* Punkt-Form */
.ct-series-d .ct-bar {stroke: blue;}
```

```javascript
var data = {
    labels: ['Mon', 'Tue', 'Wed', 'Thu'],				// x-Werte
	series: [ [15, 17, 22, 20],	[40, 50, 60, 50], ]		// y-Werte
};			//	Serie-A				Serie-B

var options = {
    fullWidth:	true,							// Diagram über ganze Breite
    showArea:	true,							// Fläche unter Graph
    showPoint:	false,							// Datenpunkte anzeigen
    showLine:	false,							// keine Datenline
    stackBars:	true,							// gestapeltes Säulendiagram
    lineSmooth: Chartist.Interpolation.none(),	// Punkte gerade verbinden
    lineSmooth: Chartist.Interpolation.step(),	// Line stufenartig
    axisY: {onlyInteger: true, offset: 20}		// ganze Zahlen, Achsenabstand
};

new Chartist.Line('.mychart', data, {fullWidth: true});		// Kurvendiagram
new Chartist.Bar('.mychart', data);							// Säulendiagram
new Chartist.Pie('.mychart', data);		// Tortendiagram → labels leer lassen
```

```html
<script>	var options = {	width: 320,	height: 180	};	</script>

<head><style type="text/css">
.mychart {	width: 320px;	}
</style></head>
<body><div class="mychart"></div></body>	<!-- keine Containerklasse
```

**D3**

```html
<head>
    <script src="//cdn.jsdelivr.net/npm/d3@6.7.0/dist/d3.min.js"></script>
</head>
<body>
    <div id="aDiv">Div 1</div>
    <script>
        d3.select("#aDiv").style("color", "blue");
    </script>
</body>
```

```javascript
d3.selectAll("p").style("color", "red");
d3.selectAll("p")
	.transition().duration(2000).style("color", "red")		// Farbe über 2s nach rot
	.transition().duration(2000).style("color", "green")	// Farbe über 2s nach grün
	.remove();												// P's von Seite entfernen
```

**JSON**

```json
{	"vorname":"Markus",
	"nachname":"Mustermann",
	"alter":25	}
```

```javascript
var person;
var xmlhttp = new XMLHttpRequest();
var url = "personendaten.json";
xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		person = JSON.parse(xmlhttp.responseText);
		writeln("Vorname: " + person.vorname);
		writeln("Nachname: " + person.nachname);
		writeln("Alter: " + person.alter);
	}
};
xmlhttp.open("GET", url, true); // true: asynchr. transfer
xmlhttp.send();
```

<div style="page-break-after: always; break-after: page;"></div>

## 6. XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE Daten SYSTEM "Daten.dtd">
<Daten>
	<element attr1="A">
        <tag1>numA</tag1>
        <tag2>dataA</tag2>
        <tag3 attr2="…"></tag3>
    </element>
    <element attr1="B">
        <tag1>numB</tag1>
        <tag2>dataB</tag2>
    </element>
</Daten>
```

```xml-dtd
			<!--(element+)						 element muss mind. einmal vorkommen -->
<!ELEMENT Daten (element*)>					<!-- element darf beliebig oft vorkommen -->
<!ELEMENT element (tag1, tag2, tag3?)>		<!-- tag3 darf einmal vorkommen -->
<!ELEMENT tag1 (1|2|3|4) "1">				<!-- standardmäßig 1 -->
<!ELEMENT tag2 (#PCDATA)>
<!ELEMENT tag3 EMPTY>
<!ATTLIST element attr1 (A|B|C) #REQUIRED>
<!ATTLIST tag3 attr2 CDATA>
```



## 7. Bedienelemente und Ereignisbehandlung

**Button**

```html
<script>
	function bHandler(name){
    document.write("Button " + name + " clicked");
	}
</script>
<button onclick="bHandler('B1')">OK</button>

<script>
	function bHandler(event) {
	document.write("Button " + event.currentTarget.id + " clicked");
	}
</script>
<button id="B1" onclick="bHandler(event)">OK1</button><br>
<button id="B2" onclick="bHandler(event)">OK2</button><br>
```

**zwei Dateien**

```javascript
var btn = document.getElementById("btn");
var output = document.getElementById("output");
btn.onclick = function () {
	output.innerHTML = "Button clicked";
}
```

```html
<head>	<script src="ButtonExample.js" defer></script>	</head>
<body>
	<div>Button Example</div>
	<button id="btn">OK</button>
	<div id="output"></div>
</body>
```

**Eingabe-Zeile**

```html
<input type="text" size="30" maxlength="40">	<!-- maxlength: max Anzahl Zeichen -->

<script>										// kopiere Text von Feld in Ausgabe
    function textHandler() {
		var str = document.getElementById("str").value;
		document.getElementById("output").value = str;
	}
</script>
<input type="text" id="str" size="30" maxlength="40">
<button onclick="textHandler()">OK</button>
<input type="text" id="output">
```

**Eingabe-Feld**

```html
<textarea id="textfeld" cols="50" rows="20"></textarea>

<script>
	function taHandlevenlyer() {
		var str = document.getElementById("taIn").value;
		document.getElementById("taOut").value = str;
	}
</script>
<textarea id="taIn" cols="50" rows="20"></textarea>
<button onclick="taHandler()">OK</button>
<textarea id="taOut" cols="50" rows="20"></textarea>
```

**Check-Box**			<img src="../../Typora.assets/Check-Box.png" alt="Check-Box" style="zoom:33%;" />

```html
<script>
function cbHandler() {
	var str = "";
	if (document.getElementById("cb1").checked)
		str += document.getElementById("cb1").value + ", ";
	if (document.getElementById("cb2").checked)
		str += document.getElementById("cb2").value + ", ";
	document.getElementById("output").innerHTML = str;
}
</script>
<input type="checkbox" id="cb1" value="cover" checked>Akku<br>
<input type="checkbox" id="cb2" value="accu">Speicher<br>	
<button onclick="cbHandler()">OK</button>
<div id="output"></div>
```

<div style="page-break-after: always; break-after: page;"></div>

**Radio Button**		<img src="../../Typora.assets/Radio Button.png" alt="Radio Button" style="zoom:33%;" />

```html
<script>
	function rbHandler() {
		var card = "";
		if (document.getElementById("rb1").checked)
			card = document.getElementById("rb1").value;
		if (document.getElementById("rb2").checked)
			card = document.getElementById("rb2").value;
		document.getElementById("output").innerHTML = card;
	}
</script>
<input type="radio" id="rb1" name="rbgroup1" value="Mastercard">Mastercard
<input type="radio" id="rb2" name="rbgroup1" value="Eurocard">Eurocard
<button onclick="rbHandler()">OK</button>
<div id="output"></div>
```

**Auswahlliste**		<img src="../../Typora.assets/Auswahlliste.png" alt="Auswahlliste" style="zoom:33%;" />

```html
<script>
	function selectHandler() {
		var selected = document.getElementById("zahlweise").value;
		document.getElementById("output").innerHTML = selected;
}
</script>
<select id="zahlweise">
	<option value="Kreditkarte">Kreditkarte</option>
	<option value="Vorauskasse">Vorauskasse</option>
	<option value="Rechnung" selected>Rechnung</option>
</select>
<button onclick="selectHandler()">OK</button>
<div id="output"></div>
```

**Flexbox**

```html
<style type="text/css">
    .hcontainer{
        display:flex;
        flex-direction:column						/* default: zeile, so: spalte */
        justify-content:flex-start|flex-end|center;	/* Ausrichtung links/rechts/mitte */
        justify-content:space-between				/* Ausrichtung: Container füllen */
    }
</style>
<div class="hcontainer">
    <input type="…" name="…">
</div>
```

<div style="page-break-after: always; break-after: page;"></div>

## 8. Allgemeine Bedienelemente und Zeitsteuerung

```javascript
setTimeout(fkt, t)				// fkt wird einmal nach t Millisek. aufgerufen
var id = setInterval(fkt, t)	// fkt wird mehrmals nach t Millisek. aufgerufen
clearInterval(id)				// beendet Aufrufen von fkt (durch id)
```



## 9. Formulare

```html
<form action="process.php" method="get">		<!-- 'novalidate' → keine Fehlerprüfung -->
	<!-- hier folgen die Formularelemente -->
    <input type="submit" value="Abschicken">	<!-- Absende-Knopf "Abschicken" -->
    <button type="submit" formnovalidate>Abschicken</button>	<!-- Fehler erlaubt -->
    <input type="reset" value="Abbrechen">		<!-- Daten verwerfen -->
    <input type="search" name="text">			<!-- Textfeld -->
    <input type="email">	<input type="url">
    <input type="number" min="1" max="5">
    <input type="date">		<input type="time">
</form>

<form onsubmit="return check()">				<!-- Prüft Formular vor Abschicken -->
<!-- hier folgen die Formularelemente -->
</form>

<input type="…" required>						<!-- Eingabe/Auswahl erforderlich -->

<output></output>
```

**Strings**

```javascript
var strA = "Example String";				// gleicher Wert, aber 
var strB = new String("Example String");	// unterschiedlicher Typ

modStr = origStr.operation(par1,...,parN);	// nach Modifikation immer neuer String
str.length									// Länge
str.toLowerCase()	str.toUpperCase()		// alle Zeichen klein/groß
str.trim()									// Leerzeichen an Anfang/Ende weg

str.replace(oldStr, newStr)					// 1. Vorkommen von oldStr durch newStr ersetz
str.replace(/oStr/g, "nStr")				// alle Vorkommen von "oStr" durch "nStr"  ''

str.indexOf(searchStr, startIndex)			// Position 1. Vorkommen, erstes Zeichen 0
str.lastIndexOf(searchStr, startIndex)		// Position l. Vorkommen, kein Vorkommen: -1
str.search(searchStr)						// Suche in Form regulärer Ausdrücke möglich

str.substr(startIndex, length)				// Zeichenkette ab startIndex Länge length
str.substring(startIndex, endIndex)			// Zeichenkette von startIndex bis endIndex
str.split(cutter)							// Feld von Teil-Strings
```

<div style="page-break-after: always; break-after: page;"></div>

**Beispiel**

```html
<script>
	function check() {
		document.getElementById("output").innerHTML = "";
		var email = document.getElementById("email").value;
		var hpage = document.getElementById("hpage").value;
		var valid = true;
		if (email.indexOf(".") < 0) {
			document.getElementById("output").innerHTML =
			"Email-Adresse ungültig: " + email + "<br>";
			valid = false;
		} if (hpage.indexOf(".") < 0) {
			document.getElementById("output").innerHTML +=
			"URL ungültig: " + hpage + "<br>";
			valid = false;
		} return valid;	}
    if (window.location.href.indexOf("?") > -1)
		document.write("Formulardaten erhalten: " + window.location.
		href.substr(window.location.href.indexOf("?")+1) + "<br>");
    if (window.location.href.indexOf("?") > -1) {
		var querystring = window.location.href.split("?");
		var entries = querystring[1].split("&");
		for (var i in entries) {
		var entry = entries[i].split("=");
		var name = decodeURIComponent(entry[0]);
		var val = decodeURIComponent(entry[1]);
		document.write(name + ": " + val + "<br>");
	}	}
</script>

<form name="bcard" onsubmit="return check()">
	Name: <input type="text" id="name" name="name"><br>
	Email: <input type="email" id="email" name="email"><br>
	Homepage: <input type="url" id="hpage" name="hpage"><br>
	<input type="submit" value="Abschicken">
</form>
<output id="output"></output>

<!-- http://localhost:8383/.../Visitenkarte.html?name=Markus+Mustermann&
email=markus%40mustermann.de&hpage=http%3A%2F%2Fwww.mustermann.de -->
```

<div style="page-break-after: always; break-after: page;"></div>

## 10. Templates

```html
<head>
    <script src="handlebars.min.js"></script>
</head>
<body>
    <div>HandleBars Example</div>
	<script id="t" type="text/x-handlebars-template">
		<p>{{firstname}} {{lastname}}</p>
		<!-- Platzhalter auskommentieren: {{!lastname}} -->
	</script>
	<div id = "name"></div>
    
	<script>
		var source = document.getElementById("t").innerHTML;
		var template = Handlebars.compile(source);
		var context = {firstname: "Mark", lastname: "Muster"};
		var output = template(context);
		document.getElementById("name").innerHTML = output;
</script>
</body>
```

**Helfer**

```html
<script id="t" type="text/x-handlebars-template">
	<p>
    	{{#if age}} {{age}}				<!-- falls Altersangabe in Daten -->
    	{{else}} Keine Altersangabe.	<!-- var context = {..., age: 32}; -->
    	{{/if}}
    <br>
    	{{#each guests}}				<!-- context = {"guests": [ -->
    		{{fName}} {{lName}}			<!-- {"fName":"Mark","lName":"Muster"}, -->
    	{{/each}}						<!-- {"fName":"Petra","lName":"Pattern"}, -->
    </p>								<!-- {"fName":"Susan","lName":"Sample"}]} -->
</script>
<div id = "name"></div>
```

**Komposition**

```html
context = { "people" : [
	{ "fName":"Mark","adress":{"location":"Munich", "street":"Maximilianstr.", "no":"1"} }
]}
{{#each people}}
	{{fName}}: {{adress.location}}, {{address.street}} {{address.no}}
{{/each}}

context = { "names" : ["Mark", "Petra"]}
{{#each names}}
	{{@index}}: {{this}},					<!-- 0: Mark, 1: Petra -->
{{/each}}
```

<div style="page-break-after: always; break-after: page;"></div>

## 11. Datenspeicherung

**Web Storage**

```javascript
// name: Objekt-Eigenschaft		value: Objekt-Wert

localStorage.setItem("name", document.getElementById("name").value);
document.getElementById("output").innerHTML = "Guten Tag " + localStorage.getItem("name");
localStorage.removeItem("name");

localStorage.val = 3.57;						// wird als String gespeichert
var val = 10 + parseFloat(localStorage.val);	// Umwandlung zu Zahlenwert
```

**Feld speichern / einlesen**

```javascript
var person = {
	vorname: document.getElementById("vorname").value,
	nachname: document.getElementById("nachname").value
}
person.val = []; // Im Objekt ein Feld anlegen
for (var i=0; i<10; i++) {
	person.val.push(i); // Feldelemente schreiben
}
localStorage.person = JSON.stringify(person);

var person = JSON.parse(localStorage.person);
var str = "Guten Tag " + person.vorname + " " + person.nachname + "<br>";
for(var i=0; i < person.val.length; i++)
	str += person.val[i] + ", ";
document.getElementById("output").innerHTML = str;
```



## 12. Verlaufsverwaltung und Audio

**Verlauf bei Single-Page**

```javascript
history.pushState(stateObject, title, URL);		// Zustands-Daten, Titel, wechselnde URL
// bei Vor-/Zurück-Button wird 'popstate'-Event ausgelöst
```

**Audio**

```html
<audio controls>
	<source src="http://adress1/title1.mp4" type='audio/mp4' />
	<source src="http://adress1/title1.oga" type='audio/ogg; codecs=vorbis' />
	<p>Audio element nicht unterstützt.</p>
</audio>	<!-- erste unterstützte Quelle wird abgespielt -->
```

```javascript
var a = new Audio();
a.addEventListener("canplaythrough", event => {
	a.play();  });		a.pause();		// Abspielen / Pausieren
```
