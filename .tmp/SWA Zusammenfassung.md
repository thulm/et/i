| KI  <img src="../../Typora.assets/image-20221010095642511.png" alt="image-20221010095642511" style="zoom:33%;" /> | Dinge tun, die Mensch bislang besser kann, Leistungen menschlichen Gehirns erlernen | Machine Learning                       | Algorithmen erkennen aus Daten Muster und treffen Vorhersagen |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------------------------- | ------------------------------------------------------------ |
| NN Aufbau                                                    | bilden Grundkonzeptions des Gehirns nach: Neuron erzeugt elektrischen Impuls oder nicht | NN Einsatz                             | Klassifikation, Erkennung von Eigenschaften                  |
| Algorithmus                                                  | methodische, mathematische Vorgehensweise um Fragestellung zu lösen | deep learning                          | verwendet künstliche neuronale Netze mit vielen Schichten    |
| starke KI                                                    | „menschlichen“ Fähigkeiten, Lösung unbekannter Aufgabe       | schwache KI                            | bestimmte Aufgabe                                            |
| Beispiele neuronales Netz                                    | Face-Recognition, Video-Spiel Bots, Chatbots, Objekterkennung, Unterschriftenerkennung | Computer Vision ohne Neuronale Netze   | Bilderkennung alg. anhand von festen Mustern, Farben etc., schwächelt bei Unregelmäßigkeiten |
| Heraus-forderungen                                           | Blickwinkel, Beleuchtung, Deformation, Verdeckungen, Hintergrund, Variation in Kl. | optimizer                              | `SGD`, `RMSProp`, `Adam`, SDG Gradient zufälligen Trainings-Beispiels |
| Arten Machine **Learning**                                   | **Supervised**: gelabelte Trainingsdaten, **Unsupervised**: Mustererkennung, **Reinforcement**: Belohnung maximieren | Convolutional<br />Neural<br />Network | Kombination verschiedener Layer, räumliche Pixel-Anordnung nutzen, Objekt-Position in Bild egal |
| Kostenfkt.<br />$s=\sum(a-y)^2\\c\!=\!\!-\!\!\!\sum y\log(a)$ | `mean_squared_error`<br />`categorical_crossentropy` – nur korrekte Labels, + <u>Computer Vision</u><br />`mean_absolute_error` + falsch gelabelt | Aktivierungs-funktion:  $\varphi$      | gewichtete Summe der Eingänge                                |
| Bias:  B / x~0~                                              | startet mit 1, sorgt dafür dass Ergebnis nicht 0 wird, Strenge der Mustererkennung | Schwellen-wert  $\theta$               | Falls  $\varphi$  $\theta$  üverschreitet wird Ergebnis weitergeleitet |
| BatchSize                                                    | BS↓ LR↑, BS↓ + faster learning, – more fluctuating           | Momentum                               | addiert m* voriges Gewicht zum aktuellen                     |
| Convolutional<br />Layer                                     | Repräsentation räumliche Verhältnisse                        | Pooling<br />Layer                     | Reduktion Bildgröße / Daten                                  |
| learning rate                                                | Schrittweite bei Gradientenabstieg (Fehler minimieren)       | Epoche                                 | kompletter Durchlauf durch Trainingsbeispiele                |
| FLOP                                                         | floating point operation, multiplication or addition         | MAC (multiply accumulate)              | convolutional operations<br />nMACs ~= 0.5 nFLOPs            |

| Inception Layer / module                  | design a good local network topology and then stack these modules on top of each other | Bottleneck Layer           | 1x1 convolution: räumliche Auflösung erhalten, Tiefe reduzieren |
| ----------------------------------------- | ------------------------------------------------------------ | -------------------------- | ------------------------------------------------------------ |
| Q-Learning                                | Reinforcement Learning Form: lernen, erwartete Belohnungen für jede Aktion in bestimmtem Zustand vorherzusagen | compound scaling           | compound scaling uniformly scales channels, resolution and depth with a fixed ratio |
| feature space                             | 8x8 Bild → 64D-Vektoren / Koordinatensystem<br />$\mu,~\sigma$ → 2D pro Bild | label space                | bspw. 1000 Klassen                                           |
| object<br />detection                     | Objekte in Bild klassifizieren & mit Box umranden            | semantic<br />segmentation | Pixel-Bereich gleichfarbig markieren, der gleicher Klasse entspricht (# egal) |
| Kontext-abhängige Banditen                | beste Aktion von Zustand abhängig, wird von Agent anhand von Zustand gewählt | Multi-Armed-Bandit Problem | kein Zustand                                                 |
| katastro-phales Vergessen                 | zu lernende Daten löschen/ verfälschen Gelerntes, Q-Funktion wird verwirrt, lernt nichts mehr | Markov-Eigenschaft         | aktueller Zustand enthält genug Infos, um beste Aktion zu wählen |
| Zustandsraum                              | alle möglichen Zustände, in denen sich Umgebung befinden kann | Aktionsraum                | alle in bestimmten Zu-stand mögliche Aktionen                |
| Zustandswert                              | erwartete Summe der Belohnungen für einen Zustand, wenn wir eine bestimmte Policy verfolgen | Aktionswert                | erwartete Belohnung für Durchführung von Aktion in bestimmtem Zustand → State-Aktion-Paar-Wert |
| On-Policy-Learning                        | Policy lernen und gleichzeitig nutzen, um Daten für das Lernen zu sammeln | Off-Policy-Learning        | Policy lernen, während wir mit einer anderen Policy Daten sammeln |
| Erfahrung basierendes Wieder-holungsspiel | ermöglicht Batch-Training von Reinforcement Learning-Alg.→ weniger katastrophales Vergessen, stabileres Training | Target-Network             | Haupt-DQN-Kopie → Stabilisierung der Aktualisierungsregel für das Training des DQN |

| [conf.matrix](https://youtu.be/_cpiuMuFj3U)<br />Accuracy | Error-Rate                  | [Precision /<br />Relevance](https://youtu.be/qWfzIYCvBqo) | [Recall /<br />Sensitivity___](https://youtu.be/qWfzIYCvBqo) |   N=10   |    predicted +    |    predicted –    |
| :-------------------------------------------------------- | :-------------------------- | ---------------------------------------------------------- | :----------------------------------------------------------- | :------: | :---------------: | :---------------: |
| $\dfrac{TP+TN}{N}$                                        | $\dfrac{FP+FN}N$            | $\dfrac{TP}{TP+FP}$                                        | $\dfrac{TP}{TP+FN}$                                          | actual + | 3: True positive  | 2: False Negative |
| richtig vorherges. von allen                              | falsch vorherges. von allen | wirklich pos. von pos. vorherges.                          | pos. vorherg. von wirklich pos.                              | actual – | 1: False Positive | 4: True Negative  |

| InputLayer        | Conv.Layer      | MaxPooling    | Flatten | FullyConn.Layer              | OutputLayer |
| ----------------- | --------------- | ------------- | ------- | ---------------------------- | ----------- |
| Eingabe Dimension | Mustererkennung | Bildreduktion |         | Klassifizierung Eingabedaten |             |

| Sigmoid                                                      | tanh                                                         | <u>Re</u>ctified <u>L</u>inear <u>U</u>nits                  | Softmax                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20230206091332453](../../Typora.assets/image-20230206091332453.png) | ![image-20230206091353617](../../Typora.assets/image-20230206091353617.png) | ![image-20230206091417286](../../Typora.assets/image-20230206091417286.png) | Für letzten/Ausgabe- Layer, Wkt.sverteilung für Eingabewerte des Neurons → Zuordnung auf Klassen |
| $sigmoid=\dfrac1{1+e^{-x}}$                                  | $\tanh=\dfrac{e^x-e^{-x}}{e^x+e^{-x}}$                       | $relu(x)=max(0,x)$                                           | $softmax(x_i)=\dfrac{e^{x_i}}{\sum_je^{x_j}}$                |
| Steigung kann ≈ 0 → + Ausreißer unterbind.<br />– vanishing gradient | + gut für dense Lay.                                         | + am besten für conv-L<br />+ Steigung ≠ 0<br />+ einfache Berechnung | + letzter dense / output Layer                               |

**0. ==[Moderne](https://www.youtube.com/watch?v=pj9-rr1wDhM) [CNNs](https://www.youtube.com/watch?v=HGwBXDKFk9I)==**

| ==Maßnahme==                                                 | Funktion                                                     | Vor- / Nachteile                                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Dropout                                                      | random Gewichte = 0<br />GoogLeNet: 0.4 bester Wert<br />cnpncnpn8x8x192in8x8x480ipdf-fc | – Lernen langsamer, + Redundanz erzwingen, + keine Koadaption von Merkmalen, + weniger Overfitting |
| Aug-mentation                                                | s/w, (flip), crop, scale, color, jitter, brightness, contrast, rotate, shift | + weniger Overfitting, – evtl. langsamerer Anstieg           |
| [Batch Norm-alization](https://www.youtube.com/watch?v=dXB-KQYkzNU) | $y=\dfrac{x-\mu}{\sigma}·\underbrace{\pmb{\gamma}+\pmb\beta}_{trainable}$<br />vor hidden layer / bsp. nach conv | + keine unverhältnismäßig großen Gewichten/ Neuronen-Werte, + train faster, + wider learn rate range, + training stability, (+ replace dropout), – stagniert schneller |
| Learning Rate anpassen                                       | LR Schrittweise über Epochen reduzieren                      | reduziert zufällige Fehler-Schwankungen<br />(untersch. Gradienten bei versch. Batches)<br />+ schneller Gewinn, – dann langs.er lernen |
| compound scaling                                             | $d=\alpha^\phi\qquad w=\beta^\phi\qquad r=\gamma^\phi\\ \alpha·\beta^2·\gamma^2\approx2\qquad\alpha,\beta,\gamma>1$<br /><u>deeper</u> (more conv layers)<br />+ richer/more complex features<br />– harder to train (vanishing gradient) | <u>wider</u> (more channels / feature maps), + easier to train, + capture fine-grained features, – saturate quickly<br /><u>higher (spatial) resolution</u>: + in theory more fine-grained details, – worse accuracy |
| Quantization                                                 | post-training (static) quantization<br />train float32, calc min/max of<br />weights/feature maps, calc S/Z<br />with calibration dataset, calc r | scale & shift: + ganzes Spektrum ausnutzen<br />+ smaller storage/download size<br />+ less memory usage, – less accuracy |



| ==Netz==<br />Layer | Param Layer             | Neu                                  | Vorteile                                                     | Nachteile                        |
| ------------------- | ----------------------- | ------------------------------------ | ------------------------------------------------------------ | -------------------------------- |
| Alex<br />8         | 60M<br />c-c-3c-3f      | ReLu, Dropout,<br />Augmentierung    | + zeigte, dass CNN funktioniert                              |                                  |
| VGG<br />16 - 19    | 138M<br />2c2c2c3c3c    | Kernel kleiner<br />Netz tiefer      | + 3mal 3x3 = rezept. Feld wie 7x7<br />bei weniger weniger Param. | – viele Parameter in Dense Layer |
| GoogLe 22           | 5M                      | Inception Modul                      | + Reduktion klassischer FC Layer<br />+ effizienter          | – viele Parallel-Zweige →komplex |
| Res<br />18-152     | bsp. 1M<br />(18 Layer) | Residual Block<br />besser als :man: | + tiefer → bessere Accuracy<br />+ übersichtl./einheitl. Design | – tiefer schwerer zu optimieren  |
| Mobile<br />NetV2   | 3.4M<br />19            | ReLU6 robuster bei Quantisierung     | + weniger Parameter, + schnellere Ausführungsgeschwindigkeit | – minimaler Accuracy Verlst      |

**2\. ==VGG==**      <img src="../../Typora.assets/20200402154036896.png" width=80% />

|            | conv3-512                                                    | pool2                                                | dense fc-4096               |
| ---------- | ------------------------------------------------------------ | ---------------------------------------------------- | --------------------------- |
| **out**    | (in + 2·padding – kernel) / stride + 1<br />= (14 + 2·1~same~ – 3) / 1 + 1 = 14 | (in – pool) / stride + 1<br />= (14 – 2) / 2 + 1 = 7 | dense<br />= 4096           |
| **memory** | outX · outY · outChannels<br />= 14 · 14 · 512 ≈ 100k        | outX · outY · outChannels<br />= 7 · 7 · 512 ≈ 25k   | dense<br />= 4096           |
| **params** | kernelX · kernelY · inChannels · outChannels<br />= filterSize · filters ≈ 3 · 3 · 512 · 512 ≈ 2M | 0                                                    | inMem·dense<br />≈ 25k · 4k |
| **MAC**    | params · outX · outY<br />≈ 2M · 14 · 14                     | 0                                                    | params<br />≈ 103k          |

<img src="../../Typora.assets/image-20230206214952433.png" width=80% />

**3. ==GoogLeNet==** <img src="../../Typora.assets/image-20230206102754289.png" width=85% />



<img src="../../Typora.assets/image-20230206113249403.png" width=50% />		...	  <img src="../../Typora.assets/image-20230206113343113.png" width=20% />		<img src="../../Typora.assets/image-20230206103032611.png" width=15% />

| 4.==ResNet==  | n            | 2n           | 6n           | 3n                              | filter_size   |
| ------------- | ------------ | ------------ | ------------ | ------------------------------- | ------------- |
| ..img size    | blocks per.. | layers per.. | total layers | total shortcuts/residual blocks | Anzahl Kanäle |
| **Beispiele** | 1, 3, 3      | 2, 6, 6      | 6, 18, 24    | 3, 9, 12                        | 64/16, 8, 64  |

<img src="../../Typora.assets/image-20230206112947966.png" width=35% /><img src="../../Typora.assets/image-20230206113646122.png" width=65% /> 

**5\. ==Transfer Learning==**

- Feintuning mit bspw. 1/10 der Lernrate von Pretraining-Datenset
- bei sehr verschiedenem Pretraining-Datenset viele Layer Tunen (viele Daten) & Augmentation (wenig Daten)

![image-20230206221502129](../../Typora.assets/image-20230206221502129.png)

| Kernel<br />Filter                        | ![image.0WFSZ1](../../Typora.assets/image.0WFSZ1.png)        | ![image.M0VGZ1](../../Typora.assets/image.M0VGZ1.png)        | ![image.UJFHZ1](../../Typora.assets/image.UJFHZ1.png)        |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Channel<br />Activation Feature<br />Maps | ![image-20230206122541184](../../Typora.assets/image-20230206122541184.png) | ![image-20230206122546117](../../Typora.assets/image-20230206122546117.png) | ![image-20230206122616006](../../Typora.assets/image-20230206122616006.png) |

| Adversarial perturbations                                    | Fooling Images, white-box-attack: adjust gradient            |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| loss_total = a · loss_content<br />+ b · loss_style<br />loss_c = difference between content image & noise (**one** feature map)<br />loss_s = difference between style image & noise (covariance of **all** feature maps of 1 layer) | [Neural Style Transfer](https://youtu.be/B22nIUhXo4E)<br /><img src="../../Typora.assets/image-20230207001710580.png" alt="image-20230207001710580" style="zoom:35%;" /> |

| 7\. ==Effiziente Netzwerke==   | most memory | meiste Parameter     | most FLOPs |
| ------------------------------ | ----------- | -------------------- | ---------- |
| Cost & Scale, Privacy, Latency | early CONV  | erste Dense/FC Layer | conv Layer |

<img src="../../Typora.assets/image.RI5LZ1.png" width=70% />

| depthwise convolution                  | pointwise convolution      | DSC / normal convolution              |
| -------------------------------------- | -------------------------- | ------------------------------------- |
| kernelX · kernelY · inCh · outX · outY | inCh · outCh · outX · outY | 1/outCh + 1/(kernelX · kernelY) ≈ 1/9 |

**MobileNetV2**  inverted residual (Kanäle innen erhöhen)

<img src="../../Typora.assets/mobilenetv2.png" width=37% />		 <img src="../../Typora.assets/image-20230206174924487.png" width=40% />

**EfficientNet** – Quantisierung

| real (float32) | quantized (int8)       | scaling factor                               | q(r=0)                              |
| -------------- | ---------------------- | -------------------------------------------- | ----------------------------------- |
| $r=S·(q-Z)$    | $q=round(\dfrac rS+Z)$ | $S=\dfrac{r_{max}-r_{min}}{q_{max}-q_{min}}$ | $Z=round(q_{max}-\dfrac{r_{max}}S)$ |

```python
image_np[j][i] = round(image_np[j][i]/reductionFactor)*reductionFactor
```

| 8.1 ==Semantic Segmentation==            Max Unpooling       | Transpose Convolution                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20230204095721380.png" width=95% /> | ![image-20230204100536522](../../Typora.assets/image-20230204100536522.png) |
| **U-Net**  (uses max unpooling or transpose convolution)     | **correspond. down-/upsampling**                             |
| ![image-20230204095541655](../../Typora.assets/image-20230204095541655.png) | ![image-20230204100420100](../../Typora.assets/image-20230204100420100.png)<br />Convolution → wird normal tiefer/ schmaler, bei gleicher Auflösung sehr teuer |

| 8.2 ==Object Detection==                                     | Learnable Region Proposals                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20230206203403794.png" alt="image-20230206203403794" style="zoom: 33%;" /> | Sliding Window → zu viele Boxen, ineffizient<br />R-CNN: Region-Based CNN<br />RoI: Region of Interest<br />Region Proposal Network (RPN)<br /><br />**First Stage <span style="color:blue">(blue)</span>**<br />- Box Position: anchor box is object/not object<br />- Box Größe: anchor box to proposal box<br />**Second Stage <span style="color:green">(green)</span><br />**- Box Klasse: classify object (or background)<br />- neue Box Größe: proposal box to object box |

| Intersection over Union                       | Many overlapping detection boxes | 1. Box wählen      | 2. Boxen eliminieren                               | 3. übrige Boxen |
| --------------------------------------------- | -------------------------------- | ------------------ | -------------------------------------------------- | --------------- |
| $IoU=\dfrac{Area~AND}{Area~OR}$  (=1 is best) | Non-Max Suppression (NMS)        | höchste Wkt: **b** | P~x~ < P~b~  &&  IoU(b, x) > threshold  (e.g. 0.7) | goto 1          |

| <img src="../../Typora.assets/image-20230204104709122.png" alt="image-20230204104709122" style="zoom:33%;" /><br /><img src="../../Typora.assets/image-20230204104544090.png" alt="image-20230204104544090" style="zoom:33%;" /> | <img src="../../Typora.assets/image-20230204163402662.png" alt="image-20230204163402662" style="zoom:30%;" /> |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| AP (Fläche unter Kurve) für jede<br />Kategorie (carAP@0.5 = 0.68) | [Mean Average Precision (mAP@IoU threshold)](https://youtu.be/FppOzcDvaDI)<br />mAP@0.5 = ⌀ carAP@0.5, dogAP@0.5, ... = 0.77 |

9\. ==Reinforcement Learning==

<img src="../../Typora.assets/image-20230204173028976.png" width=65% /><img src="../../Typora.assets/image-20230204173522814.png" width=20% />

| Fkt    | Policy                                                       | Wert                     | Aktion-Wert / Q                        |
| ------ | ------------------------------------------------------------ | ------------------------ | -------------------------------------- |
| Formel | $\pmb{\pi}:s~→~P(A\mid s)$                                   | $V_\pi:s→E(R\mid s,\pi)$ | $Q_\pi:(s,a)→E(R\mid a,s,\pi)$         |
| Input  | Zustand                                                      | Zustand                  | Zustand + Aktion                       |
| Output | Wkt der in Zustand möglichen Aktionen, dass damit größte Belohnung | erwartete Belohnung      | erwartete Belohnung /<br />Aktionswert |

- Policy Gradients: neuronales Netz, um Aktion direkt auszugeben
- Actor-Critic-Methoden: Q-Learning fällt unter die Kategorie der Wert-Methoden, da wir versuchen, Aktionswerte zu lernen, während Methoden mit einem Policy-Gradienten direkt versuchen, die besten Aktionen zu erlernen. Wir können diese beiden Techniken zu einer sogenannten Actor-Critic-Architektur kombinieren.
