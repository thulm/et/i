| ==1. Verteiltes System==                                     | Netzwerk/Sammlung autonomer Recheneinheiten (K.s), wirken wie kohärentes System (/einzelnes System) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| autonome Recheneinheiten                                     | K. haben eigene Zeit: Synchronisation und Koordinierung nötig |
| Netzwerk                                                     | Sammlung autonomer Recheneinheiten                           |
| kohärentes System                                            | Netzwerk unabhängig von Ort/Zeit/Nutzungsart                 |
| 2. Verteilungstransparenz                                    | hoch, wenn Komponenten in System unsichtbar/transparent ("Information hiding"), max. VT nicht immer sinnvoll |
| 3. Offenheit                                                 | wohldefinierte Schnittstellen, portierbar, erweiterbar       |
| 4. Skalierbarkeit                                            | Größen- (Anzahl Benutzer/Prozesse), geographische, administrative |
| Middleware<br />Häufig verwendete<br />Komponenten/Funktionen<br /> → müssen nicht von Apps<br />separat implementiert<br />werden | <img src="../../Typora.assets/image-20220722103723957.png" height=180 /> |
| C.-S. Modell                                                 | C.: initiierender Prozess, S.: reagierender Prozess (transiente synchrone Kommunikation: gleichz. aktiv, C. blockiert während S. …) |
| Proxy-S. und Cache                                           | Proxy zwischen C.s/S.s: Stellvertreter tatsächlicher Instanz, +Leistung, +Zugänglichkeit, +Verfügbarkeit erhöhen |
| Peer Prozesse                                                | Gleichrangige Prozesse (sind gleichz. C./S.), Gesamtressourcen optimieren |
| World Wide Web                                               | HTML: contents/layout, URLs: identify resources, HTTP: rules for C./S. interaction |

##### ==2. Architekturen==

| Zweistufig                | <img src="../../Typora.assets/image-20220331081927678.png" alt="image-20220331081927678" height=80 /> | Dreistufig                | <img src="../../Typora.assets/image-20220331082100809.png" alt="image-20220331082100809" height=100 /> |
| ------------------------- | ------------------------------------------------------------ | ------------------------- | ------------------------------------------------------------ |
| zeitliche Entkopplung     | zeitl. Kopplung auflösen → N.en zwischenspeichern / gehen nicht verloren | referentielle Entkopplung | Komm.Partner_Adresse muss nicht bekannt sein (Broadcast)     |
| Vertikale<br />Verteilung | Jede Schicht auf anderem S., mehr Rechenpower zu einzelner Maschine hinzufügen | horizontal<br />skalieren | mehr Rechner zu Architektur hinzufügen, Last wird verteilt   |

| Strukturiertes<br />P2P    | Datenelement ist einem Schlüssel/Index zugeordnet (key,value) K.s wissen eigene Daten + ob Nachbar gesuchte Daten enthält | Unstruktu-riertes P2P | K.s wissen eigene Daten + Weg zu Nachbar    |
| -------------------------- | ------------------------------------------------------------ | --------------------- | ------------------------------------------- |
| hash-Funktion              | "zerhackt" große Eingabemenge in eindeutige hash-Zahlenkombination | Super-peer            | broker via p2p                              |
| Flooding (für unstruktur.) | Anfrage an alle Nachbarn weiterleiten                        | Random walk (unstr.)  | Anfrage_an_zufälligen Nachbarn weiterleiten |

| Anwendungs-Schichten                                         | Applikation: Benutzer-/ext.Anwendungs-Anbindung, Verarbeitung: Funktionen, Daten |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Wrapper                                                      | übersetzt Schnittstelle in andere, ermöglicht Kommunikation über normalerweise zueinander inkompatiblen Schnittstellen |
| Broker                                                       | <img src="../../Typora.assets/image-20220722104124398.png" height=100 />   Verbindungsstück mehrerer Applikationen mit Wrappern, + weniger Wrapper als bei 1-1, - Komm. abhängig von Broker (Ausfall) |
| Kontextwechsel                                               | aktueller Prozess/Thread wird unterbrochen → zu neuem wechseln → Kontext (Register) sichern + anderen Kontext wiederherstellen<br />+ Threads gleicher Adr.Raum → KonWech ohne BS<br />– BS für Prozesswechsel nötig (teurer) |
| Dispatcher/worker model<br />Prozess empfängt<br />Arbeit → verteilt<br />diese Weiter | <img src="../../Typora.assets/image-20220722153157860.png" height=140 /> |
| Middleware Layer                                             | Dienste & (Komm-/Naming-/Security)Protokolle für viele Anwendungen bereitstellen, Daten marschaln, Skalierung/Replikation/Caching |
| Stub (RPC)                                                   | Stellvertreter für komplexeren Programmcode (nicht entwickelt, auf anderem Rechner/Speicherbereich), + Komplexität verbergen, + Zugriffstransparenz, Bsp. gRPC (Google), REST (mit JSON) |
| Parameterübergabe                                            | C./S. selbe Datentyp-Kodierung, keine Referenzen/Zeiger → keine vollst. Zugriffstransparenz |
| Socket                                                       | Paar aus C.-IP/-Port und S.-IP/-Port                         |
| Message-Queuing-System (MQ)                                  | gemeinsames N.enprotokoll, Broker wandelt N.en ggf. um       |

| Schichten-basiert                                            | Ressourcen-basiert (RESTful - REpresentational State Transfer) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20220722101221026.png" alt="image-20220722101221026" height=200 width=1200 /><br />a) klare Hierarchie, b) Bib. nutzen einander, c) wie a), Bsp. a), c) Komm.Prot. | <img src="../../Typora.assets/image-20220325085329009.png" height=150 /><br />+ einheitliche/generische Schnittstelle, + immer C.-S. Modell, + stateless, N.en sind selbst-beschreibend, + häufige Anfragen sind cachable (schneller), + wird durch http kaum geblockt, - viele Echtzeitdaten lieber per MQQT etc. (großer Overhead) |

**[Distributed Hash Tables](https://youtu.be/-UU_ugiPZ9k) (DHT)** – jeder K. hat m-bit ID (bspw. m=5:  1, 4, 9, 11, 14, 18, 20, 21, 28)

- K. ist für keys kleiner gleich der eigenen ID verantwortlich: bspw. K. 1 - Key 29, 30, 31, 1

- K. sucht wert, geht durch alle durch shortcuts verbundene Nachbarknoten und bleibt bei größtem stehen, der noch nicht über dem key liegt. Wenn dann kürzester Link über key liegt, weiß aktueller K., dass key kein eigenes K. ist und springt zu dem darüber.

| ==3. Thread==  | minimaler SW-Prozessor, Kontext: Anweisung(en), +Blockieren vermeiden, +Parallelität, +große Programme | Prozess          | SW-Prozessor, Kontext: Thread(s)             |
| -------------- | ------------------------------------------------------------ | ---------------- | -------------------------------------------- |
| direkte Kosten | tatsächlicher Wechsel und Ausführung des Handlers            | indirekte Kosten | andere Kosten (Chaos im Cache)               |
| User           | Prozess wird blockiert                                       | Kernel           | Thread_wird_blockiert                        |
| D. Image       | schreibgeschützte Vorlage mit Anweisungen zum Erstellen eines D.-Containers. | D. Container     | eig. Name, IP, Disk<br />gl. OS, CPU, memory |

| D. Daemon  | verwaltet D.-Objekte: Images, Container, Netzwerke & Volumes | D. C.     | Benutzer-D. Inter-aktion, sendet Befehle an D. Daem. |
| ---------- | ------------------------------------------------------------ | --------- | ---------------------------------------------------- |
| Persistent | N. speichern, bis Zustellung erfolgreich                     | Transient | N. bei Zustell-Fehler verwerfen                      |
| Asynchron  | Sender wartet nicht auf Empfänger-Antwort                    | Synchron  | neue N. erst, wenn Antwort kam                       |

<img src="../../Typora.assets/image-20220723092112072.png" height=100 />

| node | Finger Table              | next node q, to find key   | key is close    |
| ---- | ------------------------- | -------------------------- | --------------- |
| $p$  | $FT_p[i]=succ(p+2^{i-1})$ | $q=FT_p[j]\le k<FT_p[j+1]$ | $p<k<FT_p[1]=q$ |



| Virtualisierung | HW ändert schneller als SW, Portabilität, Isolation zur Sicherheit | Bsp        |
| --------------- | ------------------------------------------------------------ | ---------- |
| HW-Emulation    | Emulation bildet Hardware nach                               | Virtual PC |
| Anwendung       | einzelne Anw. in virtueller Umgebung                         | JVM        |
| Vollständig     | vollständig virtuelle Ressourcen (OS, Name, IP, Disk), inklusive BIOS | VirtualBox |
| Para-           | Hypervisor ersetzt BS                                        |            |
| Hardware        | unveränderte BS als Gast-Systeme ausführen                   | VirtualBox |
| Betriebssystem  | voneinander abgeschottete identische Systemumgebungen auf selbem BS-Kern (Container ist Objekt), + leichtgewichtig | D.         |

<img src="../../Typora.assets/image-20220723074011393.png" width=50% /> <img src="../../Typora.assets/image-20220723103255227.png" width=49% />

| OSI 7-5 | Anwendung/Darstellung/Sitzung | HTTP/FTP/SMTP  | ==6 Kompression== | VS          |
| ------- | ----------------------------- | -------------- | ----------------- | ----------- |
| OSI 4   | Transport (VS)                | UDP/TCP        | end-to-end        | Application |
| OSI 3   | Netzwerk (VS ab hier)         | IPv4, IPv6     | hop-by-hop        | Middleware  |
| OSI 2   | Sicherung                     | Ethernet, WLAN | point-to-point    | OS          |
| OSI 1   | Bitübertragung                | Draht, Funk    | Leitungscodierung | Hardware    |

| TCP: Haupt und Kind-Prozess für jeden C.                     | UDP: nur ein Prozess                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| zuverlässig, verbindungs-/streamorientiert, 3-Wege-Handshake, verloren Verbindungs-N.en werden wiederholt, + feste Reihenfolge mehrerer N.en, – thread/process blockiert server für Komm., – overhead | unzuverlässig, verbindungslos, – nicht geordnet, + Broadcast: mehrere können Pakete empfangen, Autonomes Fahren: falls N. länger braucht (paar Sekunden alt) nicht mehr relevant |

| MQTT       | Publish-Subscribe (senden/empfangen) Architektur mit Broker, einstellb. QoS-Stufen, benötigt bspw. TCP |
| ---------- | ------------------------------------------------------------ |
| Vort.      | + leicht: min. Overhead, + unabhängig von Datenformat (bspw. auch JSON) |
| Bsp.       | SmartHome, IoT, Mach.2Mach.,                                 |
| Aufbau     | Topic + Payload: `sensor1/temperature` + `21.4°C`            |
| Sicherheit | TLS/SSL, C.-Identifikation (Benutzer/Passwort)               |

| QoS-0 ( →Pub)        | QoS-1 (→Pub, ←PubAck)             | QoS-2 (→Pub, ←PubRec, →PubRel, ←PubComp)            |
| -------------------- | --------------------------------- | --------------------------------------------------- |
| verlorene N.en mögl. | N. muss ankommen, Duplikate mögl. | N. muss ankommen, keine Duplikate, 4-Wege-Handshake |

| ==5. Naming== (in VS) | Entitäten bezeichnen, Zugriff per Zugangspunk → wird über Adresse benannt, (eindeutige) Bezeichnung von Objekten, Erklären/Identifizieren/Lokalisieren |
| --------------------- | ------------------------------------------------------------ |
| Adresse               | physischer Name, Lokalisierung, in Kontext eindeutig, Namensraum: Baumstruktur |
| Namensauflösung       | inizialer K. nötig (DNS: www.….de, NFS: /home…, Tel: 13240, IP: …) |
| Mounting              | K.bezeichner anderen Namensraums assoziiert K. in aktuellem N.R |
| DNS: Root-S.          | Namensserver in Netzwerk an den Anfrage als erstes geroutet wird, kennt alle TLD-Namesserver (.de), leitet Anfrage in Richtung Zieldomäne weiter |
| Flache Namensgebung   | nur lokales Netzwerk, Broadcast ID → Entität muss Addr. zurücksenden, Bsp. Hardware-Adresse |

| URI_(Universal_Resource_Identifier)       | URN_(Name)                  | (Locator)                                             |
| ----------------------------------------- | --------------------------- | ----------------------------------------------------- |
| Ressource ohne Zugriffsart identifizieren | dauerhafter Name, Bsp. ISBN | spezielle URIs, identifiziert Dokument (Zugriff HTTP) |

|                           Iterativ                           |                   Iterativ servergesteuert                   |                   Rekursiv servergesteuert                   |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| <img src="../../Typora.assets/image-20220723110812969.png" height=100 /> | <img src="../../Typora.assets/image-20220723110829911.png" height=100 /> | <img src="../../Typora.assets/image-20220723110847278.png" height=100 /> |
| <img src="../../Typora.assets/image-20220723105326053.png"/> | ![image-20220723111100473](../../Typora.assets/image-20220723111100473.png) | ![image-20220723111117648](../../Typora.assets/image-20220723111117648.png) |

| ==6. Koordination==<br />physische Uhren | zählt Anzahl verstrichener Sekunden, Zeitstempel-Reihenfolge kann auf untersch. K. untersch. sein | logische Uhren (in Middleware) | zählt Ereignisse (z.B. gesendete N.en)       |
| ---------------------------------------- | ------------------------------------------------------------ | ------------------------------ | -------------------------------------------- |
| Network Time Protocol (NTP)              | $\underbrace\theta_{Abweichung}=\underbrace{t_3+\frac\delta2}_{\begin{array}~server~time\\after~response\end{array}}-t_4$ , Zeit verlangsamen/beschl./zurücksetzen | Precision Time Protocol (PTP)  | + höhere Genauigkeit<br />+ lokales Netzwerk |

| Tageszeituhr  | Zeit seit festem Datum, synchronisiert, –Schalt-sekundenanpassungen, +Zeitstempel * K. vergleichen | Monotone Uhr    | bsp `micros()`<br />+ Messng auf 1 K. |
| ------------- | ------------------------------------------------------------ | --------------- | ------------------------------------- |
| concurrent    | $a\parallel b$  muss nicht gleichzeitig, nur nicht abhängig  | happened before | $a→b$                                 |
| Transitivität | $a→b,~b→c~\Rightarrow~a→c$                                   | Berkeley Alg.   | Interne_Zeitsynchr.                   |

| Lamport                                                      | Vector  `Ta=❬ta,tb,tc❭`                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Ereignis/Senden: `ta++`, Empfangen: `tb=max(ta,tb)+1`        | Ereignis/Senden: `Ta[i]++`, Empfangen:<br />`Tb[j]=max(Ta[j],Tb[j]); Tb[i]++` |
| <img src="../../Typora.assets/image-20220723164142187.png" height=80 /> | <img src="../../Typora.assets/image-20220723165811963.png" height=80 /> |
| + $a→b$  bedingt  $L(a)<L(b)$, – $L(a)<L(b)$  nicht<br />unbedingt  $a→b$, – $L(a)=L(b)$  und  $a\ne b$  möglich | + concurrent möglich, + V(a)<V(b) bedingt a→b,<br />+ V(a)=V(b) bedingt a=b, + V(a)∥V(b) bedingt a∥b |
| `a≺b` wenn `La<Lb && (La=Lb || Na<Nb)` (alphab.)             | Ta≤Tb wenn Ta[j]≤Tb[j]    Ta<Tb wenn Ta≤Tb && Ta≠Tb<br />Ta=Tb wenn Ta[j]=Tb[j]    Ta∥Tb wenn Ta!≤Tb && Tb!≤Ta |

| Best-effort                                                  | Reliable                                                     | Fifo                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20220723200524678](../../Typora.assets/image-20220723200524678.png) | ![image-20220723200535332](../../Typora.assets/image-20220723200535332.png) | ![image-20220723194632037](../../Typora.assets/image-20220723194632037.png) |
| N.en gehen verloren                                          | N. bei erstem Empfangen an alle K.s (zuverlässig) senden     | Empfangs-Reihenfolge bei N.en gleicher K. muss stimmen, N. versch. K.s egal<br />– keine eindeutige Reihenfolge |
| **Kausale-Ordnung**                                          | **Total-Order**                                              | **FIFO-Total-Order**                                         |
| ![image-20220723194650779](../../Typora.assets/image-20220723194650779.png) | ![image-20220723200033993](../../Typora.assets/image-20220723200033993.png) | ![image-20220723195400796](../../Typora.assets/image-20220723195400796.png) |
| Broadcast-/Sende-RF pro K. muss bei anderen stimmen          | Empfangs-RF bei allen K.s gleich                             | Fifo + Total-Order                                           |

| ==**8. Konsens**== | Zuverlässigkeit trotz fehlerhafter Prozesse, sich auf Datenwert einigen |
| ------------------ | ------------------------------------------------------------ |
| Total-Order-BC     | nächste N. = zu einigender Wert, N.reihenfolge aller K.s gleich, formal gleichwertig |

| Broadcast / Multicast                | K. sendet N. an alle K.s                                     |
| ------------------------------------ | ------------------------------------------------------------ |
| Receiving vs. Delivering             | <img src="../../Typora.assets/image-20220723194033877.png" height=150 /> |
| Gossip Protokolle                    | N. bei 1. Empfangen an 3 K. weiterleiten, + gr. # K.         |
| Total-Order Broadcast implementieren | zuverlässiger Broadcast, jede N. enthält Lamport Zeitstempel, N. werden auch zurück gesendet, Hold Back- & Delivery-Queue um dazwischen Reihenfolge nach Fifo zu ordnen, Acknowledgement nach N. |
| Mutual Exclusion (Erlaubnisbasiert)  | alle anderen K. / Koordinator (zentralisiert) müssen betreten Kritischen Abschnitts erlauben, Request: Erlaubnis anfragen, Reply: Erlaubnis erteilen, Release: Kritischen Bereich verlassen |
| Election Algorithm                   | Koordinator wählen: von Hand (Dateisystem), aktiver Prozess mit höchster ID (Wahl an höhere IDs) |

| ==**7. Replikation**==      | Daten auf * N. (Replika) speichern/synchronisieren, +Zuverlässigkeit, +Fehlertoleranz, +Skalierbarkeit |
| --------------------------- | ------------------------------------------------------------ |
| Idempotenz                  | $f(x)=f(f(x))$  `abs(x)` (nicht idempotent wäre `f(x){return x+1;}`) |
| Grabstein                   | statt Löschen mit "false" labeln → unsichtbar (logischer Zeitstempel für jeden Datensatz) |
| Quorum                      | mind-# Stimmen, für gültige Wahl/Abstimmung                  |
| Read-After-Write Konsistenz | Problem: Schreiben/Lesen in/aus versch. Replikaten, sicherstellen dass C. geschriebenen Wert direkt lesen kann, ist möglich wenn w erfolgr. Schreib-, r erfolgr. Lese-Vorgänge & r+w>n (Replikate) |
| **Byzantinische Fehler** | zufallig falsches Verhalten (N./Zustand), insgesamt 3f+1 Generäle nötig, um f böswillige Generäle zu tolerieren (Bitcoin, Flugzeug, SpaceX) |
| Blockchain               | Liste von Datensätzen, Änderung eines Glieds (€ Betrag ändern) macht Änderung aller folgenden notwendig (Proof of work), Erstes Glied: Nemesis/Genesis (Bsp. Politik, Finanzwesen, Zertifikat) |
| Hash (bsp SHA-256)       | Streuwertfunktion, max. Änderung Ausgabewert (~50%) bei Eingabewert-Änderung, wird aus Nonce, Data & PrevHash (& time) gebildet |
| Nonce (Number used once) | Proof of Work: Zahl wird so lange geändert, bis Hash stimmt (Wie Kombination um Zahlenschloss öffnen) |
| digitale Signatur        | math. verbundenes Schlüsselpaar, mit priv. Schlüssel unterschreiben, +Authentizität, +asymmetrische Verschlüsselung: inhaltliche Integrität |

| Vertraulichkeit (Confidentiality)<br />Einbruchs==sicherheit== (Intrusion Protection) | Unversehrtheit (Integrity)<br />Daten-/Informations-Sicherheit (protection/security) | Verfügbarkeit (Availability)<br />Funktionssicherheit (safety) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| keine unautorisierte Einsichtnahme, **Mithören (Sniffing):** N. mithören, Authentizität (Authenticity): Sender ist legit | keine unautorisierte Veränderung, **falsche Identität (Spoofing):** falsche N. senden/empfangen, Änderung/Wiederholung (Tampering/Replay): N. während Übertragung verfälschen/N. speichern, später wieder senden, Verantwortlichkeit (Responsibility): Akteur bekannt, Bsp. Bank: Web-Adresse | Korrektheit/Zuverlässigkeit, **Unterbrechen:** Informationskanal (zer)stören, Verweigerung (Denial of Service): Dienstverweigerung bewirken, Verfügbarkeit (Availability): Schutz vor Störung/Absturz, Bsp. Ping Flooding, Distributed DoS |

| Primzahlen p/q                           | n=p·q  6-Tupel (M C K~E~ K~D~ E D)                           | Entschl.Exp e                 | Verschl.Exp d                                      |
| ---------------------------------------- | ------------------------------------------------------------ | ----------------------------- | -------------------------------------------------- |
| geheim                                   | öffentlich                                                   | öffentlich                    | geheim                                             |
| **C**ypher text                          | Schlüsseltext: transformierte Daten                          | **M**essage                   | Klartext: Originaldaten                            |
| **E**ncryption                           | Verschlüsselung: Transformation                              | **D**ecryption                | Entschlüsselung: Inverse                           |
| Trial & Error                            | wie B-F mit häufigen Passwörtern                             | Brute-Force                   | alle Schlüssel ausprobieren                        |
| Short-Cut                                | alle Parameter außer Schlüssel bekannt                       | Statisch                      | Buchstaben-/Worthäufigkeiten                       |
| rechnerische / praktische Sicherheit     | theoretisch knackbar, Rechnerzeit / Speicherplatz zu viel    | absolute Sicherheit           | knacken theoretisch unmöglich                      |
| symmetrische Verschlüsselung             | Partner haben jeweils priv-Key, –mehrjähriger Alg.wechsel nötig, Bsp https | asymmetrische Verschlüsselung | öff. &  priv. Schlüssel / **K**ey, – dauert länger |
| **A**dvanced **E**ncryption **S**tandard | verschachtelte Transformation, +Stärken * Verschl.Alg. kombiniert | RSA                           | gleichzeitig digital signieren/verschlüsseln       |
| RSA Signatur (statt m x=h(m))            | s = m^d^ mod n, m = s^e^ mod n (Signatur korrekt falls m sinnvoll) | RSA Verschl.                  | c = m^e^ mod n, m = c^d^ mod n                     |
| Hybrid                                   | synchroner priv. Schlüssel wird asynchron übertragen         | Sicherheit                    | p, q Primzahlen aus geg. n schwer, n=p·q leicht    |
| **T**ransport **L**ayer **S**ecurity/SSL | C.-to-S.-Verschl. & Integrität & Authentizität               | Zertifizierungs-Instanz       | stellt Zertifikate aus, ermöglicht Signatur        |

<img src="../../Typora.assets/image-20220724120644005.png" width=55% /> <img src="../../Typora.assets/image-20220724175215968.png" width=44% />
