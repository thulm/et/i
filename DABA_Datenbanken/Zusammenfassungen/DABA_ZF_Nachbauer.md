# FRAGEN

- gibt es Abzug, wenn man ein Statement 'zu kompliziert' schreibt

```sql
statt	SELECT COUNT(geworben_von)				SELECT COUNT(kunden_nr)
		FROM kunde;								FROM kunde
												WHERE geworben_von IS NOT NULL;
```

- Muss man bei Aggregat-Funktionen immer eigene Bezeichnung festlegen?



| Begriff                           | Bedeutung                                                    |
| --------------------------------- | ------------------------------------------------------------ |
| Entität                           | Exemplar von konkreten/abstrakten Dingen in realer Welt (konkret) |
| Entitätstyp                       | Entität im Modell (abstrakt)                                 |
| Attribut                          | Eigenschaft von Entität                                      |
| Attributwerte                     | Eigenschaftswert in realer Welt                              |
| Hauptattribut                     | ist essenziell zur Identifizierung von Entität               |
| Nebenattribut                     | unnötig für Identifizierung, nur nähere Beschreibung         |
| Beziehung                         | Zusammenhang zwischen realen Identitäten (konkret)           |
| Beziehungstyp                     | (abstrakt)                                                   |
| schwacher Entitätstyp             | keine eindeutige Identifizierung durch Attribute (Beziehung nötig)<br />bei logischem Datenmodell "erbt" er Schlüsselatribute von Koppelentitätstyp |
| Schlüsselkandidat                 | ein/mehrere Attribute, mit denen eindeutige Identifizierung möglich ist |
| Primärschlüssel                   | ein Schlüsselkandidat (am besten minimale Attribut-Anzahl)   |
| Rekursiv-Beziehungstyp            | Entitätstyp geht mit sich selbst Beziehung ein (1:C, 1:N, C:N sind nicht möglich) |
| Fremdschlüssel                    |                                                              |
| Referenzielle Integrität          | Tabelleneintrag, auf den Fremdschlüssel verweist, muss vorhanden sein |
| Triviale Funktionale Abhängigkeit | Attribut kommt sowohl auf der linken, als auch auf der Rechten Seite vor |
|                                   |                                                              |



# 2. Von der Realität zum konzeptionellen Datenmodell

| Multiplizität | Symbol                                                       | einzel [min,max] | gesamt  |
| ------------- | ------------------------------------------------------------ | ---------------- | ------- |
| zero or one   | <img src="../../Typora.assets/ERD_Notation_zero or one.png" alt="ERD_Notation_zero or one" style="zoom: 50%;" /> | [0,1]            | C       |
| only one      | <img src="../../Typora.assets/ERD_Notation_one only one.png" alt="ERD_Notation_one only one" style="zoom:50%;" /> | [1,1]            | 1       |
| zero or many  | <img src="../../Typora.assets/ERD_Notation_zero or many.png" alt="ERD_Notation_zero or many" style="zoom:50%;" /> | [0,N]            | CN (CM) |
| one or many   | <img src="../../Typora.assets/ERD_Notation_one or many.png" alt="ERD_Notation_one or many" style="zoom:50%;" /> | [1,N]            | N (M)   |





# 3. Das relationale Datenbankmodell



# 4. Das logische Datenmodell: Vom Datenmodell zur Datenbank

| 1:1                                                          | 1:n - Beziehung                                              | n:n                |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------ |
| Fremdschlüssel kann in einen von beiden Entitätstypen gesteckt werden | Fremdschlüssel aus **1** wird in Entitätstyp mit **N** als Fremdschlüssel kopiert | Koppel-Entitätstyp |
|                                                              | NULL-Wert in Fremdschlüssel nicht bei **1** möglich          |                    |
|                                                              | Schwacher Entitätstyp: Fremdschlüssel wird bei Übernahme zu Teil des Primärschlüssels |                    |



# 5. Normalisierung



# 6. Structured Query Language: SRL



## 6.1 Select: Einzelne Tabelle

```mysql
SELECT ... FROM ... WHERE ... GROUP BY ... HAVING ... ORDER BY ... ;
```

|                         | `...`——————————-                                     | Beschreibung                                                 |
| ----------------------- | ---------------------------------------------------- | ------------------------------------------------------------ |
| **SELECT**              | `attribut(e)`                                        | Angezeigte Tabellen-Spalte(n)                                |
|                         | `DISTINCT attribut(e)`                               | nur mögliche versch. Werte/Kombinationen der Spalte(n)       |
|                         | `attr1 … attr2`                                      | Mathematische Operation … mit Attributen                     |
| Alias                   | `attribut(e) AS name`                                | selbst definiert Spaltenüberschrift: name                    |
| Aggregat-<br />Funktion | `SUM(attr) / AVG(attr)`<br />`MIN(attr) / MAX(attr)` | Liefert 1 Zeile mit Spalten:  Summe / Durchschnitt<br />/ Minimum / Maximum aller Werte in Spalte |
|                         | `COUNT(*) / COUNT(attr)`<br />`COUNT(DISTINCT attr)` | Anzahl Zeilen / Anzahl Zeilen, mit `NOT NULL` in attr-Spalte<br />Anzahl möglicher versch. Werte der Spalte |
| **FROM**                | `entität`                                            | Tabellen-Name                                                |
| **WHERE**               | `attr … attr_wert`                                   | Zeilenauswahl nach ... Bedingung:̣  `(!)= < > AND OR NOT`     |
|                         | `attr IS (NOT) NULL`                                 | Überprüfen, ob Daten vorhanden sind                          |
|                         | `attr … 'YYYY-MM-DD'`                                | Datum muss in diesem Format eingetragen werden               |
|                         | `attr BETWEEN … AND …`                               | Auswahl nach Wertebereich zwischen … und …                   |
|                         | `attr IN (wert1, wert2)`                             | entspricht `attr = wert1 OR attr = wert2`                    |
|                         | `attr LIKE '%…' / '_…'`                              | `%` → String-Platzhalter  `_` → Char-Platzhalter<br />(bei gleicher Zeichenkette immer `=`) |
| **GROUP BY**            | `attribut(e)`                                        | fasst Zeilen, die in attr-Spalten gleichen Wert haben,<br />zu jeweils einer Gruppe zusammen (oft mit Aggregatfkt) |
| **HAVING**              | `Aggr.fkt </=/>/!= wert`                             | Zeilenauswahl aus Gruppierungs-Ergebnis-Spalte<br />==kein Alias in MySQL erlaubt== (in Übung schon) |
| **ORDER BY**            | `attribut(e)`                                        | Spalte(n), nach der sortiert wird (Alias erlaubt)            |
|                         | `attr1 ASC, attr2 DESC`                              | Auf- / Absteigende Sortierung (geschachtelt)<br />`ASC` wird defaultmäßig angewandt |



## 6.2 Select: mehrere Tabellen

|                         |                                                         | Beschreibung                                                 |
| ----------------------- | ------------------------------------------------------- | ------------------------------------------------------------ |
| **SELECT**              | `ent1.attr1, ent1.attr2,`<br />`ent2.attr1, ...`        |                                                              |
| **FROM**                | `ent1 CROSS JOIN ent2`                                  | normal uninteressant: jede Zeile mit jeder Zeile             |
|                         | `ent1 INNER JOIN ent2 `<br />`ON ent1.attr = ent2.attr` | Zeilen mit selbem Primär-/Fremdschlüssel werden<br /> zusammen gefasst (Kaskadierbar, Reihenfolge egal) |
|                         | `ent AS e`                                              | selbst definierter Alias, kann überall verwendet werden      |
|                         | `ent1 RIGHT JOIN ent2`                                  | von rechter Tabelle werden <u>alle</u> Zeilen übernommen     |
|                         | `ent1 LEFT JOIN ent2`                                   | von linker Tabelle werden <u>alle</u> Zeilen übernommen      |
| Self Join               |                                                         | rekursiver Beziehungstyp                                     |
| **FROM**<br />**WHERE** | `ent1, ent2`<br />`ent1.attr = ent2.attr`               | Alternative Schreibweise zu JOIN ... ON                      |



## 6.3 Unterabfragen

|           |                                             |                                        |
| --------- | ------------------------------------------- | -------------------------------------- |
| **WHERE** | `attr1 = (SELECT attr1 FROM ent WHERE ...)` | = nur, wenn aus () nur ein Wert kommt  |
|           | `attr1 IN (...)`                            | IN , wenn () mehrere Werte zurück gibt |
|           | `EXISTS (SELECT * ...)`                     |                                        |

WHERE `attr1 = (SELECT attr1 FROM ent WHERE ...)`



```mysql
FROM ent1					FROM ent1
WHERE attr1 IN				WHERE EXISTS
	(SELECT attr1				(SELECT *
    FROM ent2					FROM ent2
    WHERE attr2 ...				WHERE attr2...
    );							AND ent1.attr1 = ent2.attr1);
```



## 6.4 Structured Query Language: DML/DDL

```sql
CREATE TABLE ent1
(
	attr1 datentyp1,
	attr2 datentyp2,
    attr3 datentyp3 restriktion,		-- Restriktion z.B. NOT NULL / PRIMARY KEY
	...
    CONSTRAINT bezeichnug PRIMARY KEY (attr1, attr2, ...),		-- Primärschlüssel aus
    															-- mehreren Attributen
    CONSTRAINT bezeichnung FOREIGN KEY (ent1_attr) REFERENCES ent2(pk_attr),
    
    CONSTRAINT bezeichnung UNIQE KEY (attr1, attr2, ...),
    
    attr4 datentyp4 DEFAULT attr_wert
)
```



#### Datentypen

| Wahrheit   | String             | Kommazahl                          | Datum(+Zeit) | Medien |
| ---------- | ------------------ | ---------------------------------- | ------------ | ------ |
| `BOOLEAN`  | `VARCHAR(length)`  | `DECIMAL(stellen insg, nachkomma)` | `DATE[TIME]` | `BLOB` |
| true/false | VARCHAR(3) = 'opa' | DECIMAL(3,2) = 1,25                | '2001-06-09' |        |



#### Restriktionen

|                  |                                                              |
| ---------------- | ------------------------------------------------------------ |
| `NOT NULL`       | Werte in Spalte dürfen nicht NULL sein                       |
| `PRIMARY KEY`    | (nicht Null/Duplikat)                                        |
| `AUTO_INCREMENT` | Neue Zeile wird  automatisch mit fortlaufender Nummer erstellt |
| `FOREIGN KEY`    | siehe Beispiel oben:  PK aus ent1 wird als FK in ent2 kopiert |
| `UNIQUE`         | Attribut-Wert darf nur einmal / in nur einer Zeile verwendet werden (auch Kombination mit CONSTRAINT möglich, siehe Beispiel oben) |
| `DEFAULT`        | Wenn in Zeile nichts eingegeben wird, wird automatisch Default-Wert eingesetzt |



#### Referenzielle Integrität

```
CONSTRAINT .. FOREIGN KEY (..) REFERENCES ..(..) ON DELETE befehl1 ON UPDATE befehl2
```

befehl:	Was soll passieren, wenn eine Zeile gelöscht/umbenannt wird, auf die über einen Fremdschlüssel in einer anderen Tabelle verwiesen wird (Empfehlung Restrict)

|        | befehl = RESTRICT            | befehl = CASCADE                  | befehl = SET NULL        |
| ------ | ---------------------------- | --------------------------------- | ------------------------ |
| DELETE | Zeile mit FK bleibt erhalten | Zeilen mit FK werden gelöscht     | FK wird auf NULL gesetzt |
| UPDATE | Fehlermeldung                | Zeilen mit FK werden aktualisiert | FK wird auf NULL gesetzt |



#### VIEWS

```sql
CREATE VIEW view_name AS
select_statement_mit_zeilenauswahl

DROP VIEW view_name						-- VIEW-Tabelle wieder löschen
```



#### Daten in Tabelle bearbeiten

```sql
INSERT INTO ent (attr1, attr2, ...)
VALUES (attr_wert1, attr_wert2, ...)	-- Auch SELECT-Statement als attr_wert möglich

INSERT INTO kunde (kunden_nr, name, strasse, datum-letzte-werbeaktion)
SELECT kunden_id, name, strasse, letzte-werbeaktion
FROM kunde_alt
```

```sql
UPDATE ent
SET attr1 = attr_wert,
	attr2 = ...
[WHERE ...]					-- Zeilenauswahl
```

```sql
DELETE FROM ent				-- Zeilen leeren
[WHERE ...]					-- Zeilenauswahl
```

```sql
DROP ent		-- Tabelle löschen
DROP DATABASE	-- Datenbank löschen
```







# HÄUFIGE FEHLER:



### Logisches Datenmodell

- Beziehungs-Beschriftungen fallen weg
- Bei Fremdschlüssel auch (FK) dahinter schreiben, und nicht nur unterstreichen
- Bei schwachem Entitätstyp/Koppelentitätstyp darf kein Dreieck mehr stehen
- 1:1-Beziehung kann zu einer Entität zusammengefasst werden



### SQL

- Bei GROUP BY müssen alle SELECT-Attribute, die nicht im GROUP BY stehen in einer Aggregatfunktion stehen
- `SELECT werte ... GROUP BY werte    →    SELECT DISTINCT werte ...`    Wenn in SELECT und GROUP BY die gleichen Werte stehen, dann kann man GROUP BY weglassen und stattdessen einfach DISTINCT vor SELECT-Werte schreiben
- ==nicht immer JOIN nötig==, Aufgabe funktioniert möglicherweise auch ohne (Bsp. nur Kundennummer, nicht Kundenname)
- kein ON bei CROSS JOIN!
- Lieber eine Tabellenspalte mehr mit Zusatzinfos anzeigen
- Wenn es leicht geht, am besten SELECT *, statt bestimmten Attributen
- Wenn nach möglichen Attribut-Werten gefragt ist, immer DISTINCT !



### SQL - Aufgabenstellung

- Wörter in Aufgabenstellung wortwörtlich nehmen, bei 'Einkaufspreis' auch Attribut 'einkaufsreis' und nicht 'rechnungsbetrag' nehmen
- 'Zeigen Sie alle Bestellungen' bedeutet, alle Bestellnummern anzuzeigen
- Aufgabenstellung wortwörtlich nehmen: 'wann bestellt' → bedeutet 'bestelldatum', nicht 'lieferdatum'
- wortwörtlich nehmen: 'am häufigsten' bedeutet nach COUNT sortieren