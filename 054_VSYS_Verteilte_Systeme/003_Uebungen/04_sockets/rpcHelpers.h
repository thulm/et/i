#ifndef HEADERS_H
#define HEADERS_H
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#define PORT 8080
#define IP_ADDR "127.0.0.1"
#endif /* HEADERS_H */


enum MessageType {Request, Reply};

typedef struct {
    enum MessageType messageType;  // same size as an unsigned int
    unsigned int RPCId;		   // unique identifier
    unsigned int procedureId;	   // e.g.(1,2,3,4) for (+, -, *, /)
    int arg1;			   // argument/ return parameter
    int arg2;			   // argument/ return parameter
} RPCMessage;

#define BUF_LEN 1024
typedef struct {
    char buffer[BUF_LEN];
    unsigned int length;
} Message;

//
//implementieren sie die folgenden Funktionen, genutzt von Client und Server
//
// RPCMessage => Message
void marshal(RPCMessage* rm, Message* message);

// Message => RPCMessage
void unMarshal(RPCMessage* rm, Message* message);