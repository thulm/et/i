#include "rpcHelpers.h"

int localAdd(int arg1, int arg2) {
    return arg1 + arg2;
}

int localSub(int arg1, int arg2) {
    return arg1 - arg2;
}

int localMult(int arg1, int arg2) {
    return arg1 * arg2;
}

int localDiv(int arg1, int arg2) {
    return arg1 / arg2;
}

int main(int argc, char const *argv[]) {
    /* Open Socket (Port 8080) and listen for RPC-Calls.
    */
    int server_fd, new_socket, valread;
    struct sockaddr_in address; int opt = 1; int addrlen = sizeof(address);
    char buffer[1024] = {0};
    // SOCK_STREAM = TCP
    // AF_INET = Adressfamilie "Internet"
    server_fd = socket(AF_INET, SOCK_STREAM, 0); // Creating socket file descriptor
    // Forcefully attaching socket to the port 8080
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY; // htonl(INADDR_ANY)
    address.sin_port = htons( PORT ); // host-to-network-short
    // Forcefully attaching (bind) socket to the port 8080
    bind(server_fd, (struct sockaddr *)&address, sizeof(address));
    listen(server_fd, 3); // listen

    // Preparing memory
    Message message_send, message_recv;
    RPCMessage recvRPCMsg, sendRPCMsg;
    unsigned int lastRPCId = 0;
    unsigned int listenerInstance = 0;
    // reopen/unblock 'new_socket' for every new client arriving
    while(1) {
        listenerInstance++;
        new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
        printf("%03d>> listening for new client.\n", listenerInstance);
        valread = read(new_socket, message_recv.buffer, 1024);
        // process all rpc calls sent by the client as long as their socket is opened
        while(valread > 0) {
            unMarshal(&recvRPCMsg, &message_recv);
            printf(
                    "%03d>> received message: %d, procedure: %d/%d arg1: %d arg2: %d\n",
                    listenerInstance,
                    valread, // length of recieved buffer
                    recvRPCMsg.RPCId,
                    lastRPCId,
                    recvRPCMsg.arg1,
                    recvRPCMsg.arg2
                    );
            if(recvRPCMsg.RPCId > lastRPCId || 1) {
                lastRPCId = recvRPCMsg.RPCId;
                // start building the response
                sendRPCMsg.messageType = Reply;
                sendRPCMsg.RPCId = recvRPCMsg.RPCId;
                sendRPCMsg.procedureId = 0;
                sendRPCMsg.arg1 = 0;
                sendRPCMsg.arg2 = 0;
                // executing the local functions
                if(recvRPCMsg.procedureId  == 1) {
                    sendRPCMsg.arg1 = localAdd(recvRPCMsg.arg1, recvRPCMsg.arg2);
                } else if(recvRPCMsg.procedureId  == 2) {
                    sendRPCMsg.arg1 = localSub(recvRPCMsg.arg1, recvRPCMsg.arg2);
                } else if(recvRPCMsg.procedureId  == 3) {
                    sendRPCMsg.arg1 = localMult(recvRPCMsg.arg1, recvRPCMsg.arg2);
                } else if(recvRPCMsg.procedureId  == 4) {
                    sendRPCMsg.arg1 = localDiv(recvRPCMsg.arg1, recvRPCMsg.arg2);
                } else {

                }

                marshal(&sendRPCMsg, &message_send);
                int ret = send(new_socket, message_send.buffer, message_send.length, 0);
                printf("%03d<< sent message. (send-returncode %d)\n", listenerInstance, ret);
                //close(new_socket);
            } else {
                printf("%03d== RPC-Ids did not match.\n", listenerInstance);
                close(new_socket);
            }
            valread = read(new_socket, message_recv.buffer, 1024);
        }
    }
    return 0;
}
