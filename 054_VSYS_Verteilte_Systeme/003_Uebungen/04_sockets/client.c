#include "rpcHelpers.h"
#include "time.h"

/**
- returns the addition operation of arg1 and arg2
- builds up a to a server which will reply the operation
- marshals the given arguments
- sends the data, waits and receives the reply
- unmarshals the receided data
- return the result to the main function*/
unsigned int RPCId = 1;

void rpcSetupSocket(int* sock) {
    *sock = 0;
    struct sockaddr_in serv_addr;
    // Creating socket file descriptor
    *sock = socket(AF_INET, SOCK_STREAM, 0);
    // assign IP, PORT
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    // Convert IPv4 and IPv6 addresses from text to binary form
    inet_pton(AF_INET, "141.59.42.54", &serv_addr.sin_addr);
    int retval = connect(*sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)); //connect
    printf("connect val %d\n", retval);
}

void rpcCloseSocket(int* sock) {
    close(*sock);
}

void rpcCallnResp(int* sock, RPCMessage* sendRPCM, RPCMessage* recvRPCM) {
    sendRPCM->messageType = Request;
    sendRPCM->RPCId = RPCId;

    RPCId += 1;

    Message message_send, message_recv;
    marshal(sendRPCM, &message_send);

    printf("before send");
    int retval = send(*sock, message_send.buffer, message_send.length, 0);
    printf("send-retval %d\n", retval);
    int valread = read(*sock, message_recv.buffer, 1024);
    unMarshal(recvRPCM, &message_recv);
}

int remoteAdd(int* sock, int arg1, int arg2) {
    RPCMessage rpcS, rpcR;
    rpcS.procedureId = 1;
    rpcS.arg1 = arg1;
    rpcS.arg2 = arg2;

    rpcCallnResp(sock, &rpcS, &rpcR);
    return rpcR.arg1;
}

int remoteSub(int* sock, int arg1, int arg2) {
    RPCMessage rpcS, rpcR;
    rpcS.procedureId = 2;
    rpcS.arg1 = arg1;
    rpcS.arg2 = arg2;

    rpcCallnResp(sock, &rpcS, &rpcR);
    return rpcR.arg1;
};

int remoteMult(int* sock, int arg1, int arg2) {
    RPCMessage rpcS, rpcR;
    rpcS.procedureId = 3;
    rpcS.arg1 = arg1;
    rpcS.arg2 = arg2;

    rpcCallnResp(sock, &rpcS, &rpcR);
    return rpcR.arg1;
};

int remoteDiv(int* sock, int arg1, int arg2) {
    RPCMessage rpcS, rpcR;
    rpcS.procedureId = 4;
    rpcS.arg1 = arg1;
    rpcS.arg2 = arg2;

    rpcCallnResp(sock, &rpcS, &rpcR);
    return rpcR.arg1;
};


int main() {
    /*
    Ask for arguments to be worked with and make Remote-Procedure Calls to the corresponding server
    */
    int socket = 0;
    rpcSetupSocket(&socket);

    printf("Which operation should we request from the server?\n");
    printf("Definition: 1:+   2:-   3:*   4:/   5: run1000times\n");
    printf("Please insert 1, 2, 3, 4 or 5: ");
    int op;
    scanf("%i",&op);
    printf("\nPlease insert arg1: ");
    int arg1;
    scanf("%i",&arg1);
    printf("\nPlease insert arg2: ");
    int arg2;
    scanf("%i",&arg2);
    switch(op) {
        case 1:
            printf("Remote addition result %d \n", remoteAdd(&socket, arg1, arg2));
            break;
        case 2:
            printf("Remote substraction result %d \n", remoteSub(&socket, arg1, arg2));
            break;
        case 3:
            printf("Remote multiplication result %d \n", remoteMult(&socket, arg1, arg2));
            break;
        case 4:
            printf("Remote division result %d \n", remoteDiv(&socket, arg1, arg2));
            break;
        case 5:
            for(int i = 0; i<arg1; i++) {
                remoteAdd(&socket, i*5, i*3);
            }
            break;
        default:
            printf("Operation not supported \n");
    } 

    rpcCloseSocket(&socket);
    return 0;
}