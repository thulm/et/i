#include "Chord.h"

ChordNode::ChordNode(const uint32_t id){
    init(id);
}

void ChordNode::init(const uint32_t id){
    this->m_id = id;
    this->m_predecessor = NULL;
    for(uint32_t i=0; i < M+1;i++){
        this->m_fingerTable[i] = NULL;
    }
}

// Print Finger Table
void ChordNode::printFingerTable() {
    cout << "\x1B[32m";
    cout << "**** Node ID : " << this->m_id << " ****\n";
    // cout << "\nFingerTable\n";
    cout << 0 << " : " << "predcsor : " ;
    if(NULL != m_fingerTable[0])
        cout << m_fingerTable[0]->m_id << "\n";
    else
        cout << "NULL" << "\n";

    for (uint32_t i = 1; i < M+1; i++) {
        uint32_t idFTpI = (this->m_id + (uint32_t)pow(2, i-1))%(uint32_t)pow(2,M);
        cout << i << " : " << "succ(" << idFTpI << ") : " ;
        if(NULL != m_fingerTable[i])
            cout << m_fingerTable[i]->m_id << "\n";
        else
            cout << "NULL" << "\n";

    }
    // cout << "\nKeys : ";
    // for (map<int, int>::iterator iter = m_local_keys.begin();
    //      iter != m_local_keys.end(); iter++) {
    //     cout << iter->second << "  ";
    // }
    // cout << "\n**********************\n";
    cout << "\033[0m";
}

ChordNode* ChordNode::findSuccessor(const uint32_t id) {
    //special case no better information, return own node
    if(m_predecessor == this && m_fingerTable[1] == this){
        return this;
    }
    //key is between pred+1 and self id
    //pred+1 <= key <= self_id
    if(inbetween(id,m_predecessor->getId()+1,this->getId()+1)) {//self_id+1 because of impl. in inbetween
        return this;
    } else {
        //id is not in range we need to search at the succ
        //slow version ask the succ
        return m_fingerTable[1]->findSuccessor(id);
    }
}

void ChordNode::updateFingerTable() {
    // update finger table
    // fingerTable[1] is always the successor
    for (uint32_t i = 1; i < M+1; i++) {
        //FTp[i] = succ(p + 2^(i−1))
        // at i we search for succ(idFTpI)
        // idFTpI = p + 2^(i−1)
        uint32_t idFTpI = (this->m_id + (uint32_t)pow(2, i-1)) % (uint32_t)pow(2,M);
        m_fingerTable[i] = this->findSuccessor(idFTpI);
    }
}

void ChordNode::join(ChordNode* node) {
    // 1st special case: First node to join
    if (node == NULL) {  // First node to join
        for (int i = 0; i < M+1; i++) {
            m_fingerTable[i] = this;
        }
        m_predecessor = this;
    }
    else{
        // Find successor to attach to
        ChordNode* succ = node->findSuccessor(this->m_id);//search for the own id at the given node
        //ChordNode* succ = node->findKey(this->m_id);//search for the own id at the given node

        // Update node's successor to point to the successor
        m_fingerTable[1] = succ;

        // Update predecessor to successor's old predecessor
        m_predecessor = succ->m_predecessor;
        m_fingerTable[0] = succ->m_predecessor;

        // Update successor's predecssor to self
        succ->m_predecessor = this;
        succ->m_fingerTable[0] = this;

        // Update predecssor's successor to self
        m_predecessor->m_fingerTable[1] = this;

        // move keys on the successor before changing predecessor
        //moveKeys(succ, id);

        this->updateFingerTable();
        succ->updateFingerTable();
        m_predecessor->updateFingerTable();
    }
}


bool ChordNode::inbetween(const uint32_t key, const uint32_t lwb, const uint32_t upb){
    if (lwb <= upb)
      return lwb <= key && key < upb;
    else
      return (lwb <= key && key < upb + TWO_POW_M-1) ||
             (lwb <= key + TWO_POW_M-1 && key < upb);
}

ChordNode* ChordNode::findKey(const uint32_t key){
    cout << "Search for key " << key << " at node: " << this->m_id << endl;
    //printFingerTable();
    
    if(inbetween(key,m_predecessor->getId()+1,this->getId()+1)) {
        cout << "Found the key!" << endl;
        return this;
    }
    else
    {
        //
        //
        //Hier implementieren sie ihre Lösung
        //bisher wird immer nur zum nächsten Nachbarn gegangen
        int index = 0;
        

        for(int i=0;i<M;i++) {
            if(inbetween(key, m_fingerTable[i]->getId()+1, m_fingerTable[i+1]->getId()+1)) {
                index = i+1;
                //cout << i << "|" << i+1 << "true" << endl;
                break;
            } else {
                //cout << i << "|" << i+1 <<"false" << endl;
            }
        }

        cout << "chosen index: " << index << endl;
        ChordNode* nextNode = m_fingerTable[index];

        cout << "Goto next node " << nextNode->getId() << endl;
        return nextNode->findKey(key);
    }
}
