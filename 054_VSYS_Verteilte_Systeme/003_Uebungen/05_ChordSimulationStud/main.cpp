#include <iostream>
#include "Chord.h"

using namespace std;

int main()
{
    const uint32_t numberNodes = 9;
    ChordNode nodeArray[numberNodes];
    int idArray[numberNodes] = {1, 4, 9, 11, 14, 18, 20, 21, 28};
    for(uint32_t i=0; i<numberNodes;i++) {
        nodeArray[i].init(idArray[i]);
    }
    nodeArray[0].join(NULL);
    for(uint32_t i=1; i<numberNodes;i++) {
        nodeArray[i].join(&nodeArray[i-1]);
    }
    for(uint32_t i=0; i<numberNodes;i++){
        nodeArray[i].updateFingerTable();
        //nodeArray[i].printFingerTable();
    }
    
 cout << "search key------------------------" << endl;
    nodeArray[0].findKey(26);

    cout << "search key------------------------" << endl;
    nodeArray[8].findKey(12);

    cout << "search key------------------------" << endl;
    nodeArray[1].findKey(31);
    return 0;
}
