#include <iostream>
#include <map>
#include <math.h>

using namespace std;
// number of bits, max for this impl. is 32, due to internal datastructures
#define M 5
// often used 2^M
#define TWO_POW_M 32

class ChordNode {
private:
    uint32_t m_id; // id of the node, between 0..2^M

    //our key,value store, we currently do not use this
    std::map<int, int> m_local_keys;

    //direct link to predecessor
    ChordNode* m_predecessor;

    //m_fingerTable[0] -> predecessor
    //m_fingerTable[1] -> direct successor, next node in ring
    //m_fingerTable[2] -> i:2 -> succ(m_id + 2^(i-1))
    //              ...
    //m_fingerTable[M] -> i:M -> succ(m_id + 2^(i-1))
    ChordNode* m_fingerTable[M+1]; //because in fingerTable[0] we save the predecessor


public:
    //Default constructor
    ChordNode() {}
    //Parameterized constructor, uses internal method init()
    ChordNode(const uint32_t id);
    //Destructor, important for the map datastructure which saves the keys
    ~ChordNode() {
        //Removes all elements from the map container (which are destroyed),
        //leaving the container with a size of 0.
        m_local_keys.clear();
    }

    void init(const uint32_t id);
    uint32_t getId(){return m_id;}
    void printFingerTable();
    bool belongsToNode(const uint32_t id);
    ChordNode* findSuccessor(const uint32_t id);
    void updateFingerTable();
    void join(ChordNode* node);
    bool inbetween(const uint32_t key, const uint32_t lwb, const uint32_t upb);
    ChordNode* findKey(const uint32_t id);

};



