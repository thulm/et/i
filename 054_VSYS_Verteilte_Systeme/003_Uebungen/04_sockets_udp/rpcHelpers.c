#include "rpcHelpers.h"

void marshal(RPCMessage* rm, Message* message) {
    /*
    derialize RPC-Messace into space-padded string of the components
    */
    sprintf(message->buffer, "%d %d %d %d %d\n", rm->messageType, rm->RPCId, rm->procedureId, rm->arg1, rm->arg2);
    message->length = strlen(message->buffer);
}

void unMarshal(RPCMessage* rm, Message* message) {
    /*
    deserialize a message buffer back into the original RPCMessage-structure
    */
    sscanf(message->buffer, "%d %d %d %d %d", &rm->messageType, &rm->RPCId, &rm->procedureId, &rm->arg1, &rm->arg2);
    message->length = strlen(message->buffer);
}