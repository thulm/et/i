#include "rpcHeaders.h"


void main() {

    RPCMessage foo;
    RPCMessage baz;
    Message bar;


    foo.messageType = Request;
    foo.RPCId = 10;
    foo.procedureId = 1;
    foo.arg1 = 10;
    foo.arg2 = 20;

    marshal(&foo, &bar);
    unMarshal(&baz, &bar);


    printf("%d|%d\n", baz.arg1, baz.arg2);
}