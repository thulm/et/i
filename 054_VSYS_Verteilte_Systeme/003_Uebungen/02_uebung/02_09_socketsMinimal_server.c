 #include "02_09_socketsMinimal_headers.h"
int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address; int opt = 1; int addrlen = sizeof(address);
    char buffer[1024] = {0};
    server_fd = socket(AF_INET, SOCK_STREAM, 0); // Creating socket file descriptor
    // Forcefully attaching socket to the port 8080
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    address.sin_family = AF_INET; address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    // Forcefully attaching (bind) socket to the port 8080
    bind(server_fd, (struct sockaddr *)&address, sizeof(address));
    listen(server_fd, 3); // listen
    //accept
    new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
    while(1) {
        valread = read( new_socket , buffer, 1024);
        printf("Received message: %s\n",buffer );
        sprintf(buffer,"Hello from server!");
        send(new_socket , buffer , strlen(buffer) , 0 );
    }
    return 0;
}
