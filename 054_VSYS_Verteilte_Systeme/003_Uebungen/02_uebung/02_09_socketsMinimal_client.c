#include "02_09_socketsMinimal_headers.h"
int main(int argc, char const *argv[]) {
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    sock = socket(AF_INET, SOCK_STREAM, 0);// Creating socket file descriptor
    // assign IP, PORT
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    // Convert IPv4 and IPv6 addresses from text to binary form
    inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);
    connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)); //connect
    send(sock , hello , strlen(hello) , 0 ); //send data
    while(1) {
        printf("Which message should we send to the server: \n");
        fgets(buffer, 1024, stdin);
        send(sock , buffer , strlen(buffer) , 0 );
        printf("Your message was sent, wait for the reply.\n");
        valread = read( sock , buffer, 1024);
        printf("%s\n",buffer );
    }
    return 0;
}
