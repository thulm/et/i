## 1. Was bedeutet Verteilungstransparenenz? 
Beschreiben sie den Begriff mit ihren eigenen Worten.

## 2. Beispiele von verteilten Systeme die sie kennen und selbst benutzen oder benutzt haben:
### Nennen sie 3 Beispiele.
### Bewerten sie die Beispiele anhand der Entwurfsziele verteilter Systeme.
Entwurfsziele:
- Gemeinsame Ressourcen
- Verteilungstransparenz
- Offenheit
- Skalierbarkeit
- Fallstricke

Beispiele:
- Google-Docs
- Bahnstrecke
- 