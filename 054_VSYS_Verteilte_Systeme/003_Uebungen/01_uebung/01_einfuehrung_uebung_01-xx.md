## 1. Was bedeutet Verteilungstransparenenz? 
Beschreiben sie den Begriff mit ihren eigenen Worten.

## 2. Beispiele von verteilten Systeme die sie kennen und selbst benutzen oder benutzt haben:
### Nennen sie 3 Beispiele.
### Bewerten sie die Beispiele anhand der Entwurfsziele verteilter Systeme.
Entwurfsziele eines verteilten Systems:
- Gemeinsame Ressourcen
- Verteilungstransparenz ("Unsichtbarkeit" von Lokation, Migration, Replikation, Mehrbenutzer…)
- Offenheit
- Skalierbarkeit

Beispiele:
- Cloud-Anbieter
    - Ressourcen: Server werden unter Usern geteilt
    - Transparenz: Man merkt nicht, dass Server im Hintergrund verteilt sind
    - Offen: Für viele Menschen nutzbar
    - Skalierbar: Neue Server ergänzen die Rechenleistung
- ÖPNV
    - Ressourcen: Strukturen werden von vielen Bürgern genutzt
    - Transparenz: Lokation liegt offen aber Migration (Fahrzeugeinsatz), Verstärkerfahrzeuge (Replikation) etc. sind transparent
    - Offen: Barrierefreier zugang, einheitliche schnittstellen (Blindenleitsystem, Haltestellenkap, Beschilderung)
    - Skalierbarkeit: Neue Linien/Haltestellen unabhängig voneinander einbringbar

## 4. Verteilungsdiagramm
![01_einfuehrung_uebung_04_bild.png]()

 siehe 01_einfuehrung_uebung_04.plantuml

## 5. Verteilungsdiagramm
Erstellen sie ein Verteilungsdiagramm für Folie 47 aus der Vorlesung, jeder auf der Folie erwähnte Sensor oder Aktor ist ein eigenes Node, zusätzlich gibt es noch einen Zentralrechner mit dem Namem Intelligent-Drive-Controller (IDC).
![01_einfuehrung_uebung_05_bild.png]()

## 6. SmartHome
Architektur eines Smart-Homes aufbauen:

    Folgende Sensoren werden verwendet -> Fensterkontakt, Bewegungsmelder, Kamera, Wettersensor, Stromsensor
    Folgende Aktoren können verwendet werden -> Rollladen, Schaltbare Steckdosen, Schaltbare Lichter, Heizungs-Thermostat
    Folgende Nodes können verwendet werden -> Raspberry Pi, WLAN Router, NAS (Network Attached Storage) System, Smartphone, Mail-Server, Waschmaschine 
    Als Start können sie folgendes Diagramm nehmen -> Link
    Aufgaben:
    Erstellen sie ein UML Verteilungsdiagramm für das Smart-Home. Verbinden sie das Smart-Home mit dem Internet (das Internet wird als Wolke gezeichnet). Zeichnen sie Sensoren und Aktoren als Rechtecke. Verbinden sie die Elemente sinnvoll miteinander.
    Erstellen sie ein UML Sequenzdiagramm für folgende Systemelemente:
        Benachrichtigung auf das Smartphone, dass die Waschmaschine fertig ist.
        a) Für den Fall, wir haben eine "smarte" Waschmaschine mit Internetzugang.
        b) Für den Fall, wir haben eine alte Waschmaschine (Hinweis: nutzen sie einen Raspi und den Stromsensor). 
        Benachrichtigung auf das Smartphone, dass ein Fenster geöffnet wurde.
        Wenn der Bewegungsmelder etwas detektiert oder ein Fenster geöffnet wurde, mit der Kamera ein Bild machen und dieses auf dem NAS speichern. Was ist wenn es dunkel ist? Was könnte man zusätzlich machen?
        Wenn ein Fenster geöffnet wurde, die Heizung im selben Raum abdrehen.
